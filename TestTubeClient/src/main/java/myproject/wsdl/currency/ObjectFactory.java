//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.12.14 at 11:19:31 AM EET 
//


package myproject.wsdl.currency;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the myproject.wsdl.currency package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: myproject.wsdl.currency
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeployResultsResponse }
     * 
     */
    public DeployResultsResponse createDeployResultsResponse() {
        return new DeployResultsResponse();
    }

    /**
     * Create an instance of {@link DeployResults }
     * 
     */
    public DeployResults createDeployResults() {
        return new DeployResults();
    }

}
