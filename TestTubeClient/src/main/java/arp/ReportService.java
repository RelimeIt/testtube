package arp;

import arp.ReportClasses.Report;
import arp.ReportClasses.Test;
import org.junit.Assert;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by beckmambetov on 12/8/2015.
 */
public class ReportService {

    protected static String projectKey;
    protected static ConcurrentHashMap<String, String> sessionPath = new ConcurrentHashMap<>();
    protected static ConcurrentHashMap<String, Report> reports = new ConcurrentHashMap<>();
    protected static String parentCaller;
    protected static boolean screenshotLogging = false;
    protected static boolean jUnitLogging = false;
    protected static boolean maximizeWindowForScreenshot = false;
    protected static String reportFolderPath;
    /*
     * This parameter is used to overload behavoiur of saveScreenshot method.
     * If user wishes to overload screenshot mechanism he should init this field with
     * object from which screenshot method invocation should be used
     */
    protected static Object invoker;
    /* This is the method which would be invoked if initialiazied together with invoker.
     * It should take string - path to file and save a screenshot on that path
     * Code which generates path to file
     * sessionPath.get(currentThreadName) + "\\" + UUID.randomUUID() + ".png"
     */
    protected static Method invokable;
    private static String lastCallerThreadName = "";

    public static void setInvoker(Object invoker) {
        ReportService.invoker = invoker;
    }

    public static void setInvokable(Method invokable) {
        ReportService.invokable = invokable;
    }

    public static synchronized void setLastCallerThreadName(String name) {
        lastCallerThreadName = name;
    }

    public static synchronized String getLastCallerThreadName() {
        return lastCallerThreadName;
    }

    public static void turnOnMaximizeWindowForScreenshot() {
        maximizeWindowForScreenshot = true;
    }

    public static void turnOffMaximizeWindowForScreenshot() {
        maximizeWindowForScreenshot = false;
    }

    protected static void buildEnvironemntInfo() throws Exception {
        try {
            String currentThreadName = Thread.currentThread().getName();
            Report report = reports.get(currentThreadName);
            report.EnvironmentInformation = new arp.ReportClasses.EnvironmentInformation();
            report.EnvironmentInformation.FreeDiskSpace = getTotalFreeSpace("C:\\") / 1024 / 1024;
            report.EnvironmentInformation.Ip = getIp();
            reports.put(currentThreadName, report);

        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public static void turnOnJUnitLogging() {
        jUnitLogging = true;
    }

    public static void turnOffJUnitLogging() {
        jUnitLogging = false;
    }

    protected static String getIp() throws Exception {
        String localIp = "?";
        try {
            InetAddress inet = InetAddress.getLocalHost();
            InetAddress[] host = InetAddress.getAllByName(inet.getCanonicalHostName());
            if (host != null) {
                for (int i = 0; i < host.length; i++) {
                    if (host[i].getClass().toString().indexOf("Inet4Address") > -1) {
                        localIp = host[i].getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return localIp;
    }

    protected static long getTotalFreeSpace(String driveName) throws Exception {
        try {
            File[] paths;
            paths = File.listRoots();
            for (File path : paths)
                if (path.exists() && path.getPath().indexOf(driveName) != -1) {
                    long usableSpace = path.getUsableSpace();
                    return usableSpace;
                }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return -1;
    }

    public static void close() throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.EnvironmentInformation.EndTime = new Date();
        report.ClosedAt = new Date();
        report.CurrentTestCase = null;
        report.CurrentStep = null;
        File file = new File(sessionPath.get(currentThreadName) + File.separator + "Report.xml");
        FileOutputStream fop = new FileOutputStream(file);
        byte[] contentInBytes = arp.Serializer.Serializer.convertFromObjectToXML(report).getBytes();
        fop.write(contentInBytes);
        fop.flush();
        fop.close();
        String zipFileName = sessionPath.get(currentThreadName) + File.separator + "Report.zip";
        zipIt(zipFileName, sessionPath.get(currentThreadName));
        Path path = Paths.get(zipFileName);
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
            arp.service.DeployService currencyService = context.getBean(arp.service.DeployService.class);
            currencyService.DeployResults(Files.readAllBytes(path));
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        reports.put(currentThreadName, report);
    }

    public static void justClose() throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.EnvironmentInformation.EndTime = new Date();
        report.ClosedAt = new Date();
        report.CurrentTestCase = null;
        report.CurrentStep = null;
        File file = new File(sessionPath.get(currentThreadName) + File.separator + "Report.xml");
        FileOutputStream fop = new FileOutputStream(file);
        byte[] contentInBytes = arp.Serializer.Serializer.convertFromObjectToXML(report).getBytes();
        fop.write(contentInBytes);
        fop.flush();
        fop.close();
        String zipFileName = sessionPath.get(currentThreadName) + File.separator + "Report.zip";
        zipIt(zipFileName, sessionPath.get(currentThreadName));
        reports.put(currentThreadName, report);
    }

    public static void closeAndSendToAnotherURL(String url) throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.EnvironmentInformation.EndTime = new Date();
        report.ClosedAt = new Date();
        report.CurrentTestCase = null;
        report.CurrentStep = null;
        File file = new File(sessionPath.get(currentThreadName) + File.separator + "Report.xml");
        FileOutputStream fop = new FileOutputStream(file);
        byte[] contentInBytes = arp.Serializer.Serializer.convertFromObjectToXML(report).getBytes();
        fop.write(contentInBytes);
        fop.flush();
        fop.close();
        String zipFileName = sessionPath.get(currentThreadName) + File.separator + "Report.zip";
        zipIt(zipFileName, sessionPath.get(currentThreadName));
        Path path = Paths.get(zipFileName);
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
            arp.service.DeployService currencyService = context.getBean(arp.service.DeployService.class);
            Field x = currencyService.getClass().getDeclaredField("webServiceTemplate");
            x.setAccessible(true);
            WebServiceTemplate wst = (WebServiceTemplate) x.get(currencyService);
            wst.setDefaultUri(url);
            x.set(currencyService, wst);
            currencyService.DeployResults(Files.readAllBytes(path));
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        reports.put(currentThreadName, report);
    }

    private static void zipIt(String OUTPUT_ZIP_FILE, String SOURCE_FOLDER) throws Exception {
        try {
            ArrayList<String> fileList = new ArrayList<String>();
            generateFileList(SOURCE_FOLDER, fileList);
            byte[] buffer = new byte[1024];
            FileOutputStream fos = new FileOutputStream(OUTPUT_ZIP_FILE);
            ZipOutputStream zos = new ZipOutputStream(fos);
            for (String file : fileList) {
                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);
                FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                int len;
                while ((len = in.read(buffer)) > 0) {
                    zos.write(buffer, 0, len);
                }
                in.close();
            }
            zos.closeEntry();
            zos.close();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    private static void generateFileList(String SOURCE_FOLDER, ArrayList<String> fileList) throws Exception {
        try {
            File node = new File(SOURCE_FOLDER);
            File[] subNode = node.listFiles();
            for (File filename : subNode) {
                if (filename.isFile()) {
                    String temp = filename.getAbsoluteFile().toString();
                    fileList.add(temp.substring(SOURCE_FOLDER.length() + 1, temp.length()));
                }
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public static void newTest() throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        if (report.CurrentTestCase != null) {
            List<arp.ReportClasses.Step> steps = report.CurrentTestCase.Steps;
            for (int i = 0; i < steps.size(); i++) {
                if (steps.get(i).Status == null)
                    steps.get(i).Status = "no run";
                steps.get(i).StartTime = new Date();
                steps.get(i).EndTime = new Date();
            }
            report.CurrentTestCase = null;
        }
        NextStep();
        report.CurrentTestCase.IsStarted = true;
        report.CurrentTestCase.StartTime = new Date();
        reports.put(currentThreadName, report);

    }


    /**
     * If
     *
     * @param fileName
     * @return fileName
     * @throws Exception
     */
    public static String saveScreenshot(String fileName) throws Exception {
        try {
            if (ReportService.invoker != null && ReportService.invokable != null) {
                invokable.invoke(invoker, fileName);
                return fileName;
            }
            File f = new File(fileName);
            if (f.exists()) {
                f.delete();
            }
            if (maximizeWindowForScreenshot) {
                Thread.sleep(2000);
            }
            BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            if (maximizeWindowForScreenshot) {
                Robot x = new Robot();
                x.keyPress(KeyEvent.VK_WINDOWS);
                x.keyPress(KeyEvent.VK_DOWN);
                x.keyRelease(KeyEvent.VK_DOWN);
                x.keyRelease(KeyEvent.VK_WINDOWS);
                x.keyPress(KeyEvent.VK_WINDOWS);
                x.keyPress(KeyEvent.VK_DOWN);
                x.keyRelease(KeyEvent.VK_DOWN);
                x.keyRelease(KeyEvent.VK_WINDOWS);
            }
            ImageIO.write(image, "png", new File(fileName));
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return fileName;
    }

    public static void captureScreenShot() throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        try {
            String fileName = sessionPath.get(currentThreadName) + "\\" + UUID.randomUUID() + ".png";
            saveScreenshot(fileName);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String dateStr = dateFormat.format(date);
            File dir = new File(fileName);
            String tempStr = dir.getName();
            if (report.CurrentStep.Attachments == null)
                report.CurrentStep.Attachments = new ArrayList<arp.ReportClasses.Attachment>();
            arp.ReportClasses.Attachment att = new arp.ReportClasses.Attachment();

            att.FileName = dateStr;
            att.GuidName = tempStr;
            att.IsScreenShot = true;
            report.CurrentStep.Attachments.add(att);
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        reports.put(currentThreadName, report);
    }

    public static void addAttachment(String path) throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        try {
            File f = new File(path);
            if (!f.exists())
                return;
            String newFileName = sessionPath.get(currentThreadName) + "\\" + UUID.randomUUID() + "." + f.getName().substring(f.getName().lastIndexOf(".") + 1, f.getName().length());
            copyFile(path, newFileName);
            if (report.CurrentStep.Attachments == null)
                report.CurrentStep.Attachments = new ArrayList<arp.ReportClasses.Attachment>();
            arp.ReportClasses.Attachment attachment = new arp.ReportClasses.Attachment();
            attachment.FileName = path.substring(path.lastIndexOf("\\") + 1, path.lastIndexOf("."));
            attachment.GuidName = newFileName.substring(newFileName.lastIndexOf("\\") + 1, newFileName.length());
            attachment.IsScreenShot = false;
            report.CurrentStep.Attachments.add(attachment);
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        reports.put(currentThreadName, report);
    }

    public static void reportAction(String message, boolean isValid) throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        try {
            if (report.CurrentStep == null)
                NextStep();
            if (report.CurrentStep.Actions == null)
                report.CurrentStep.Actions = new ArrayList<arp.ReportClasses.ReportAction>();
            Boolean status = false;
            arp.ReportClasses.ReportAction action = new arp.ReportClasses.ReportAction();
            action.PublicKey = UUID.randomUUID();
            action.LoggedAt = new Date();
            action.Message = message;
            action.Valid = isValid;
            report.CurrentStep.Actions.add(action);
            report.CurrentStep.Status = "pass";
            for (arp.ReportClasses.ReportAction actions : report.CurrentStep.Actions) {
                if (actions.Valid == false) {
                    report.CurrentStep.Status = "fail";
                    if (screenshotLogging) {
                        captureScreenShot();
                    }
                }
                report.CurrentStep.ActualResult = actions.Message;
            }
            if (jUnitLogging) {
                Assert.assertTrue(isValid);
            }
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        reports.put(currentThreadName, report);
    }

    private static String getParentCaller(int frame) throws Exception {
        try {
            StackTraceElement[] allFrames = new Exception().getStackTrace();
            for (StackTraceElement stackFrame : allFrames) {
                Method[] methods = Class.forName(stackFrame.getClassName()).getDeclaredMethods();
                for (Method method : methods) {
                    Annotation[] customAttributes = method.getAnnotations();
                    for (Annotation customAttribute : customAttributes) {
                        if (customAttribute.annotationType().toString().indexOf("Annotations.AutoTest") > -1)
                            return method.getName();
                    }
                }
            }
            return null;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public static void NextStep() throws Exception {
        nextStepAction(2);
    }

    public static void nextStep() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.CurrentTestCase.Steps.set(report.CurrentStepIndex, report.CurrentStep);
        report.CurrentStepIndex = report.CurrentStepIndex + 1;
        if (report.CurrentStepIndex >= report.CurrentTestCase.Steps.size()) {
            return;
        } else {
            report.CurrentStep = report.CurrentTestCase.Steps.get(report.CurrentStepIndex);
        }
        reports.put(currentThreadName, report);
    }

    private static void nextStepAction(int frame) throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        StackTraceElement[] allFrames = new Exception().getStackTrace();
        parentCaller = allFrames[frame].getMethodName();
        if (report.CurrentTestCase == null) {
            for (arp.ReportClasses.Test test : report.TestSuite.Tests) {
                if (Objects.equals(test.Name, parentCaller)) {
                    report.CurrentTestCase = test;
                }
            }
            if (report.CurrentTestCase == null) {
                parentCaller = allFrames[frame + 1].getMethodName();
                for (arp.ReportClasses.Test test : report.TestSuite.Tests) {
                    if (Objects.equals(test.Name, parentCaller)) {
                        report.CurrentTestCase = test;
                    }
                }
            }
        } else {
            if (!Objects.equals(report.CurrentTestCase.Name, parentCaller)) {
                for (arp.ReportClasses.Test test : report.TestSuite.Tests) {
                    if (Objects.equals(test.Name, parentCaller)) {
                        report.CurrentTestCase = test;
                    }
                }
                report.CurrentStep = report.CurrentTestCase.Steps.get(0);
                report.CurrentStep.StartTime = new Date();
                report.CurrentStepIndex = 1;
                return;
            }
        }
        if (report.CurrentStep == null) {
            report.CurrentStep = report.CurrentTestCase.Steps.get(0);
            report.CurrentStep.StartTime = new Date();
            report.CurrentStepIndex = 1;
        } else if (report.CurrentTestCase.Steps.size() != report.CurrentStepIndex) {
            report.CurrentStep.EndTime = new Date();
            if (report.CurrentStep.Actions.size() == 0) report.CurrentStep.Status = "no run";
            report.CurrentStep = report.CurrentTestCase.Steps.get(report.CurrentStepIndex);
            report.CurrentStep.StartTime = new Date();
            report.CurrentStepIndex++;
            report.CurrentStep.Actions = new ArrayList<>();
        }
        if (report.CurrentTestCase.Steps.size() == report.CurrentStepIndex)
            report.CurrentStep.EndTime = new Date();
        reports.put(currentThreadName, report);
    }

    private static void copyFile(String inputFile, String outputFile) throws Exception {
        try {
            InputStream inStream = null;
            OutputStream outStream = null;
            File afile = new File(inputFile);
            File bfile = new File(outputFile);
            inStream = new FileInputStream(afile);
            outStream = new FileOutputStream(bfile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = inStream.read(buffer)) > 0)
                outStream.write(buffer, 0, length);
            inStream.close();
            outStream.close();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    private static Test findElementByName(ArrayList<arp.ReportClasses.Test> list, String name) throws Exception {
        try {
            for (arp.ReportClasses.Test el : list)
                if (Objects.equals(el.Name, name))
                    return el;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
        return null;
    }

    public static void finishTest() throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.CurrentTestCase.EndTime = new Date();
        if (report.CurrentStep.Actions == null || report.CurrentStep.Actions.size() == 0) {
            report.CurrentStep.Status = "no run";
        }
        if (report.CurrentStepIndex != report.CurrentTestCase.Steps.size()) {
            for (arp.ReportClasses.Step s : report.CurrentTestCase.Steps) {
                if (s.Status == null) {
                    s.Status = "no run";
                    s.StartTime = new Date();
                    s.EndTime = new Date();
                }
            }
        }
        report.CurrentStep = null;
        report.CurrentTestCase = null;
        report.CurrentStepIndex = 0;
        reports.put(currentThreadName, report);

    }

    public static void finishTestUnexpected(String errorMasseges) throws Exception {
        captureScreenShot();
        reportAction(errorMasseges, false);
    }

    public static void enableScreenshotLogging() {
        screenshotLogging = true;
    }

    public static void disableScreenshotLogging() {
        screenshotLogging = false;
    }

    public static void setReportFolderPath(String path) {
        ReportService.reportFolderPath = path;
    }

}
