package arp;

import arp.ReportClasses.*;

import java.io.File;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by beckmambetov on 10/28/2016.
 */
public class FlowReport extends ReportService {

    public static void open(String projectKey, String testSuite) throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report;
        if (reports == null) {
            reports = new ConcurrentHashMap<>();
        }
        CucumberArpReport.projectKey = projectKey;
        if (!(projectKey != null && !projectKey.isEmpty())) {
            throw new Exception("Project Key is not found.");
        }
        if (testSuite == null) {
            throw new Exception("Test Suite cannot be null");
        }
        report = new Report();
        reports.put(currentThreadName, report);
        report.StartedAt = new Date();
        report.ProjectKey = projectKey;
        String OS = System.getProperty("os.name").toLowerCase();
        boolean isUnix = OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0;
        sessionPath.put(currentThreadName, isUnix ? (reportFolderPath == null ? System.getProperty("java.io.tmpdir"):reportFolderPath)  + File.separator + UUID.randomUUID() : (reportFolderPath == null ? System.getProperty("java.io.tmpdir"):reportFolderPath)  + UUID.randomUUID());
        new File(sessionPath.get(currentThreadName)).mkdir();
        buildEnvironemntInfo();
        report.EnvironmentInformation.StartTime = new Date();
        report.EnvironmentInformation.StartTime = new Date();
        report.StartedAt = new Date();
        reports.put(currentThreadName, report);
    }

    public static void initFlow(ResultSet tests, Class caller) throws SQLException {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.TestSuite = new TestSuite();
        report.TestSuite.Name = caller.getName();
        report.TestSuite.Description = "";
        report.TestSuite.Tests = new ArrayList();
        report.TestSuite.DeliveredAt = new Date();
        int[] columnIndexes = getIndexes(tests);
        while (tests.next()) {
            Test test = new Test();
            test.Name = tests.getString(columnIndexes[0]);
            test.Description = tests.getString(columnIndexes[1]);
            test.Priority = tests.getString(columnIndexes[2]);
            test.Steps = new ArrayList<>();
            report.TestSuite.Tests.add(test);
        }
        tests.beforeFirst();
        reports.put(currentThreadName, report);
    }

    public static void startTest(String name) {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.CurrentTestCase = report.TestSuite.Tests.stream().filter(p -> p.Name.equals(name)).collect(Collectors.toList()).get(0);
        report.CurrentTestCase.IsStarted = true;
        report.CurrentTestCase.StartTime = new Date();
        report.CurrentStepIndex = 0;
        report.CurrentStep = null;
        reports.put(currentThreadName, report);
    }

    private static int[] getIndexes(ResultSet tests) throws SQLException {
        ResultSetMetaData rsmd = tests.getMetaData();
        List<String> columns = new ArrayList<>();
        for (int i = 0; i < rsmd.getColumnCount(); i++) {
            columns.add(rsmd.getColumnName(i + 1));
        }
        int[] columnIndexes = new int[3];
        columnIndexes[0] = columns.indexOf("TestCaseName") + 1;
        columnIndexes[1] = columns.indexOf("Description") + 1;
        columnIndexes[2] = columns.indexOf("BusinessCriticality") + 1;
        return columnIndexes;
    }

    public static void addStep(String description, String expectedResult) {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        if (report.CurrentStep != null) {
            report.CurrentStep.EndTime = new Date();
            report.CurrentStep = decideStepStatus(report.CurrentStep);
            report.CurrentTestCase.Steps.add(report.CurrentStep);
        }
        report.CurrentStep = new Step();
        report.CurrentStep.StartTime = new Date();
        report.CurrentStep.Description = description;
        report.CurrentStep.ExpectedResult = expectedResult;
        reports.put(currentThreadName, report);
    }

    private static Step decideStepStatus(Step step) {
        if (step.Actions == null || step.Actions.size() == 0)
            step.Status = "no run";
        if (step.Actions.stream().allMatch(p -> p.Valid)) {
            step.Status = "pass";
        } else {
            step.Status = "fail";
        }
        return step;
    }

    private static Test decideTestStatus(Test test) {
        if (test.Steps.stream().allMatch(p -> p.Status.equals("pass"))) {
            test.Status = "pass";
        } else {
            test.Status = "fail";
        }
        return test;
    }

    public static void finishTest() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.CurrentStep.EndTime = new Date();
        report.CurrentStep = decideStepStatus(report.CurrentStep);
        report.CurrentTestCase.Steps.add(report.CurrentStep);
        for (int i = 0; i < report.TestSuite.Tests.size(); i++) {
            if (report.TestSuite.Tests.get(i).Name.equals(report.CurrentTestCase.Name)) {
                report.TestSuite.Tests.set(i, report.CurrentTestCase);
                break;
            }
        }
        report.CurrentTestCase.EndTime = new Date();
        tieLooseEnds();
        report.CurrentTestCase = decideTestStatus(report.CurrentTestCase);
        reports.put(currentThreadName, report);
    }

    private static void tieLooseEnds() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        if (report.CurrentTestCase.Steps == null) {
            report.CurrentTestCase.Steps = new ArrayList<>();
        }
        if (report.CurrentTestCase.Steps.size() == 0) {
            Step x = new Step();
            x.StartTime = new Date();
            x.EndTime = new Date();
            x.Description = "No steps in test";
            x.Status = "no run";
            report.CurrentTestCase.Steps.add(x);
        }
        for (Step x : report.CurrentTestCase.Steps) {
            if (x.Actions == null) {
                x.Actions = new ArrayList<>();
            }
            if (x.Actions.size() == 0) {
                ReportAction y = new ReportAction();
                y.Valid = false;
                y.Message = "no actions in step";
                y.LoggedAt = new Date();
                x.Actions.add(y);
            }
        }
        reports.put(currentThreadName, report);
    }

    public static void nextStep() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.CurrentStepIndex++;
        reports.put(currentThreadName, report);
    }
}
