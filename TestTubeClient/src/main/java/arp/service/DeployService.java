package arp.service;

import arp.wsdl.currency.DeployResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

/**
 * Created by Anna on 01.04.2015.
 */
@Service
public class DeployService {

    @Autowired
    public WebServiceTemplate webServiceTemplate;

    public void DeployResults(byte[] fileContent) {
        DeployResults deployResults = new arp.wsdl.currency.ObjectFactory().createDeployResults();
        deployResults.setFileContent(fileContent);
        arp.wsdl.currency.DeployResultsResponse response = (arp.wsdl.currency.DeployResultsResponse) webServiceTemplate.marshalSendAndReceive(
                deployResults);
    }
}
