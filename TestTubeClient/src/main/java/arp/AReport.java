package arp;

import arp.ReportClasses.Report;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
public abstract class AReport extends ReportService {

    public static void open(Object testSuite) throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        Class type1Report = testSuite.getClass();
        Annotation autoReport = null;
        if (type1Report.isAnnotationPresent(arp.Annotations.AutoReport.class)) {
            autoReport = type1Report.getAnnotation(arp.Annotations.AutoReport.class);
            projectKey = (String) autoReport.getClass().getMethod("ProjectName").invoke(autoReport);
        }
        if (!(projectKey != null && !projectKey.isEmpty())) {
            throw new Exception("Project Key is not found.");
        }
        if (testSuite == null) {
            throw new Exception("Test Suite cannot be null");
        }
        report = new arp.ReportClasses.Report();
        report.StartedAt = new Date();
        report.ProjectKey = projectKey;
        sessionPath.put(currentThreadName,(reportFolderPath == null ? System.getProperty("java.io.tmpdir"):reportFolderPath) + UUID.randomUUID());
        new File(sessionPath.get(currentThreadName)).mkdir();
        reports.put(currentThreadName,report);
        buildEnvironemntInfo();
        report.EnvironmentInformation.StartTime = new Date();
        Class type = testSuite.getClass();
        Annotation autoTest = null;
        if (type.isAnnotationPresent(arp.Annotations.AutoTestSuite.class)) {
            autoTest = type.getAnnotation(arp.Annotations.AutoTestSuite.class);
        }
        if (autoTest == null) {
            throw new Exception("AutoTestSuite attribute was not declared.");
        }

        report.TestSuite = new arp.ReportClasses.TestSuite();
        report.TestSuite.Name = (String) autoTest.annotationType().getMethod("Name").invoke(autoTest);
        report.TestSuite.Description = (String) autoTest.annotationType().getMethod("Description").invoke(autoTest);
        report.StartedAt = new Date();
        ArrayList<arp.ReportClasses.Test> tests = new ArrayList<arp.ReportClasses.Test>();
        Method[] methodsInformation = type.getMethods();
        for (Method methodInfo : methodsInformation) {
            if (methodInfo.isAnnotationPresent(arp.Annotations.AutoTest.class)) {
                arp.ReportClasses.Test test = new arp.ReportClasses.Test();
                test.Name = methodInfo.getName();
                Annotation testInfo = methodInfo.getAnnotation(arp.Annotations.AutoTest.class);
                test.Description = (String) testInfo.annotationType().getMethod("Description").invoke(testInfo);
                test.Priority = (String) testInfo.annotationType().getMethod("Priority").invoke(testInfo);
                test.AuthorId = (String) testInfo.annotationType().getMethod("AuthorId").invoke(testInfo);
                ArrayList<arp.ReportClasses.Step> stepsToAdd = new ArrayList<arp.ReportClasses.Step>();
                Annotation stepsInfo = methodInfo.getAnnotation(arp.Annotations.AutoSteps.class);
                Annotation[] testStepsInfo = (Annotation[]) stepsInfo.annotationType().getMethod("value").invoke(stepsInfo);
                for (Annotation attribute : testStepsInfo) {
                    arp.Annotations.AutoStep stepInfo = (arp.Annotations.AutoStep) attribute;

                    arp.ReportClasses.Step step = new arp.ReportClasses.Step();
                    step.PublicKey = UUID.randomUUID();
                    step.Description = stepInfo.Description();
                    step.ExpectedResult = stepInfo.Expected();
                    stepsToAdd.add(step);
                }
                test.Steps = stepsToAdd;

                tests.add(test);
                report.TestSuite.Tests = tests;
            }
        }
        reports.put(currentThreadName, report);
    }


}

