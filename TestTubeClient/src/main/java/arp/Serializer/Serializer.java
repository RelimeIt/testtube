package arp.Serializer;

import arp.ReportClasses.Report;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by grachova on 12/24/2014.
 */
public class Serializer {

    public static String convertFromObjectToXML(Object object) throws Exception
    {
        JAXBContext context = JAXBContext.newInstance(object.getClass());
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        m.marshal(object, os);
        return os.toString();
    }

    public static Report convertFromXMLToObject(String xmlfile) throws Exception
    {
        JAXBContext jc = JAXBContext.newInstance(Report.class);
        Unmarshaller u = jc.createUnmarshaller();
        ByteArrayInputStream is = new ByteArrayInputStream(xmlfile.getBytes());
        return (Report) u.unmarshal(is);
    }
}
