package arp;


import arp.ReportClasses.Report;
import arp.ReportClasses.Test;
import arp.ReportClasses.TestSuite;
import cucumber.api.Scenario;
import cucumber.runtime.junit.ExecutionUnitRunner;
import cucumber.runtime.junit.JUnitReporter;
import gherkin.formatter.model.Step;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by beckmambetov on 12/7/2015.
 */
public class CucumberArpReport extends ReportService {

    protected static String _parentCaller;

    public static boolean checkIfFurtherStepsAreNeeded() {
        try {
            List<arp.ReportClasses.Step> passedSteps = CucumberArpReport.getReport().CurrentTestCase.Steps.stream().filter(p -> p.Actions != null && p.Actions.size() > 0).collect(Collectors.toList());
            if (passedSteps.stream().anyMatch(p -> !p.Status.equals("pass"))) {
                try {
                    ReportService.reportAction("Step is blocked by further step fails", false);
                    CucumberArpReport.nextStep();
                    return true;
                } catch (Throwable e) {
                }
            }
            return false;
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static String getTestSuiteName() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        return report.TestSuite.Name;
    }

    public static TestSuite getTestSuite() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        return report.TestSuite;
    }

    public static Report getReport() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        return report;
    }


    public static void setReport(Report report) {
        String currentThreadName = Thread.currentThread().getName();
        report = reports.get(currentThreadName);
        report = report;
        reports.put(currentThreadName, report);
    }

    public static void open(String projectKey, String testSuite) throws Exception {
        String currentThreadName = Thread.currentThread().getName();
        Report report;
        if (reports == null) {
            reports = new ConcurrentHashMap<>();
        }
        CucumberArpReport.projectKey = projectKey;
        if (!(projectKey != null && !projectKey.isEmpty())) {
            throw new Exception("Project Key is not found.");
        }
        if (testSuite == null) {
            throw new Exception("Test Suite cannot be null");
        }
        report = new Report();
        reports.put(currentThreadName, report);
        report.StartedAt = new Date();
        report.ProjectKey = projectKey;
        String OS = System.getProperty("os.name").toLowerCase();
        boolean isUnix = OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0;
        sessionPath.put(currentThreadName, isUnix ? (reportFolderPath == null ? System.getProperty("java.io.tmpdir"):reportFolderPath)  + File.separator + UUID.randomUUID() : (reportFolderPath == null ? System.getProperty("java.io.tmpdir"):reportFolderPath)  + UUID.randomUUID());
        new File(sessionPath.get(currentThreadName)).mkdir();
        buildEnvironemntInfo();
        report.EnvironmentInformation.StartTime = new Date();
        report.EnvironmentInformation.StartTime = new Date();
        report.StartedAt = new Date();
        testSuiteForming(testSuite);
        reports.put(currentThreadName, report);
    }

    private static void testSuiteForming(String testSuite) {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.TestSuite = new TestSuite();
        report.TestSuite.Name = (testSuite.split(";").length > 1 ? testSuite.split(";")[0] : testSuite).substring(0, 50 > (testSuite.split(";").length > 1 ? testSuite.split(";")[0] : testSuite).length() ? (testSuite.split(";").length > 1 ? testSuite.split(";")[0] : testSuite).length() : 50).replace("\r", "").replace("\n", "");
        if ((testSuite.split(";").length > 1 ? testSuite.split(";")[0] : testSuite).length() > 50) {
            report.TestSuite.Name = report.TestSuite.Name + "...";
        }
        report.TestSuite.Description = "";
        report.TestSuite.Tests = new ArrayList();
        reports.put(currentThreadName, report);
    }


    public static void addTestToTestSuite(Scenario scenario) throws NoSuchFieldException, IllegalAccessException {
        Field reporterField = scenario.getClass().getDeclaredField("reporter");
        reporterField.setAccessible(true);
        JUnitReporter reporter = (JUnitReporter) reporterField.get(scenario);
        Field runnerField = reporter.getClass().getDeclaredField("executionUnitRunner");
        runnerField.setAccessible(true);
        ExecutionUnitRunner runner = (ExecutionUnitRunner) runnerField.get(reporter);
        Test test = new Test();
        test.Name = scenario.getName();
        test.IsStarted = true;
        test.StartTime = new Date();
        fillTestWithStepsAndAdd(test, runner);
        CucumberArpReport.addTest(test);
        CucumberArpReport.setLastAddedTestActive();
    }

    private static void fillTestWithStepsAndAdd(Test test, ExecutionUnitRunner runner) {
        List<Step> stepsFromRunner = runner.getRunnerSteps();
        List<arp.ReportClasses.Step> steps = new ArrayList();
        for (gherkin.formatter.model.Step step : stepsFromRunner) {
            arp.ReportClasses.Step reportStep = new arp.ReportClasses.Step();
            reportStep.Description = step.getName();
            reportStep.PublicKey = UUID.randomUUID();
            reportStep.ExpectedResult = "";
            reportStep.StartTime = new Date();
            reportStep.EndTime = new Date();
            steps.add(reportStep);
        }
        test.Steps = steps;
    }


    public static void addTest(Test test) {

        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.TestSuite.Tests.add(test);
        reports.put(currentThreadName, report);
    }

    public static void setLastAddedTestActive() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.CurrentTestCase = report.TestSuite.Tests.get(report.TestSuite.Tests.size() - 1);
        report.CurrentStep = report.CurrentTestCase.Steps.get(0);
        report.CurrentStep.Actions = new ArrayList();
        reports.put(currentThreadName, report);
    }

    public static void decideTestStatus() {
        String currentThreadName = Thread.currentThread().getName();
        Report report = reports.get(currentThreadName);
        report.CurrentTestCase.Status = report.CurrentTestCase.Steps.stream().allMatch(p -> p.Status != null && p.Status.equals("pass")) ? "pass" : "fail";
        reports.put(currentThreadName, report);
    }
}