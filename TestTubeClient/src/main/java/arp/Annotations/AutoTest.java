package arp.Annotations; /**
 * Created by grachova on 12/24/2014.
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AutoTest
{
    public String Description() default "";
    public String Priority() default "";
    public String AuthorId() default "";
}