package arp.Annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by grachova on 1/5/2015.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface AutoReport {
    public String ProjectName() default "";
}
