package arp.Annotations;

/**
 * Created by grachova on 12/24/2014.
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AutoTestSuite
{
    public String Name() default "";
    public String Description() default "";
}
