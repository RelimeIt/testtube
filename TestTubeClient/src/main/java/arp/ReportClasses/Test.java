package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class Test
{
    public UUID PublicKey;
    public String Name;
    public String Description;
    public String Priority;
    public String AuthorId;
    public boolean IsStarted;
    public String Status;
    public Date StartTime;
    public Date EndTime;
    public List<Step> Steps;



}
