package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class Step {

    public UUID PublicKey;
    public Date StartTime;
    public Date EndTime;
    public ArrayList<Attachment> Attachments;
    public String Description;
    public String ExpectedResult;
    public String ActualResult;
    public String Status;
    public String AnalysisStatus;
    public ArrayList<arp.ReportClasses.ReportAction> Actions;

}
