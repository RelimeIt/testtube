package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class CustomStatuses {
    public UUID PublicKey;
    public String StatusName;
    public int Priority;
    public String Description;
    public String Color;

}
