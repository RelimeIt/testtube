package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class Attachment {

    public String FileName;
    public String GuidName;
    public Boolean IsScreenShot;

}
