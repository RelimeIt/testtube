package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class Analyze {

    public UUID PublicKey;
    public String CurrentDefects;
    public String OldDefects;
    public String Comments;

}
