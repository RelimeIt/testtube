package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class EnvironmentInformation
{
    public UUID PublicKey;
    public Date StartTime;
    public Date EndTime;
    public String Ip;
    public String OsVersion;
    public String Url;
    public String Browser;
    public String Environment;
    public String Account;
    public Integer FreeMemory;
    public Long FreeDiskSpace;
    public UUID BelongsToTestSuite;

}
