package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class AutoAnalysis {
    public UUID PublicKey;
    public int BelongToProject;
    public int BelongToRelease;
    public String  TestSetName;
    public String TestCaseName;
    public String Defects;
    public String Message;
    public String Comments;
    public String AnalyzedStatus;

}

