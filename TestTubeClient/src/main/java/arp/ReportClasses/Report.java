package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class Report {

    public Date StartedAt;
    public Date ClosedAt;
    public EnvironmentInformation EnvironmentInformation;
    public TestSuite  TestSuite;
    public String ProjectKey;
    public Test CurrentTestCase;
    public Step CurrentStep;
    public int CurrentStepIndex;


}
