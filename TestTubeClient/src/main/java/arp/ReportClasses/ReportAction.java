package arp.ReportClasses;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by grachova on 12/24/2014.
 */
@XmlRootElement
public class ReportAction {
    public UUID PublicKey;
    public String Message;
    public boolean Valid;
    public Date LoggedAt;
    public String AnalyzedStatus;
    public ArrayList<Analyze> ActionsAnalysis;

}
