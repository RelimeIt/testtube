package arp.Tests;


import arp.AReport;
import org.junit.Test;

/**
 * Created by grachova on 12/25/2014.Zz
 */


@arp.Annotations.AutoReport(ProjectName = "MileWeb")
@arp.Annotations.AutoTestSuite(Name = "TestSteps debug - 02072014 1640")
public class TESTs extends Template {

    @Test
    @arp.Annotations.AutoTest(Description = "Validate search query at main page - 02072014 1640")
    @arp.Annotations.AutoSteps({
            @arp.Annotations.AutoStep(Description = "A", Expected = "Login is entered"),
            @arp.Annotations.AutoStep(Description = "B", Expected = "Password is entered"),
            @arp.Annotations.AutoStep(Description = "C", Expected = "Button is clicked. Dashboard is opened.")
    })
    public void Test1() throws Exception {
        try {
            AReport.newTest();
            AReport.reportAction("1", true);
            AReport.reportAction("2", true);
            AReport.reportAction("3", true);
            AReport.reportAction("4", true);
            AReport.NextStep();
            AReport.reportAction("JaVa5", true);
            AReport.NextStep();
            AReport.reportAction("JaVa5", true);
        }

    catch (Exception ex)
    {
        AReport.finishTestUnexpected(ex.getMessage());
    }
    finally {
        AReport.finishTest();
    }

    }


    @Test
    @arp.Annotations.AutoTest(Description = "Validate search query at main page - 02072014")
    @arp.Annotations.AutoSteps({
            @arp.Annotations.AutoStep(Description = "Acdvvbv", Expected = "Login is entered"),
            @arp.Annotations.AutoStep(Description = "Bxcvbcvb", Expected = "Password is entered"),
            @arp.Annotations.AutoStep(Description = "Ccvbcbvb", Expected = "Button is clicked. Dashboard is opened.")
    })
    public void Test2() throws Exception {
        try {
            AReport.newTest();
            AReport.reportAction("1zdfg", true);
            AReport.reportAction("2dzrft", true);
            AReport.reportAction("3zdgh", true);
            AReport.reportAction("4zdgh", true);
            AReport.NextStep();
            AReport.reportAction("JaVa5srgzg", true);
            AReport.NextStep();
            AReport.reportAction("JaVa5sdsdf", true);
        }
        catch (Exception ex)
        {
            AReport.finishTestUnexpected(ex.getMessage());
        }
        finally {
            AReport.finishTest();
        }
    }


    @Test
    @arp.Annotations.AutoTest(Description = "Validate search query at main page - 02072014")
    @arp.Annotations.AutoSteps({
            @arp.Annotations.AutoStep(Description = "Adgbhfyh", Expected = "Login is entered"),
            @arp.Annotations.AutoStep(Description = "Bdfdg", Expected = "Password is entered"),
            @arp.Annotations.AutoStep(Description = "Cdfg", Expected = "Button is clicked. Dashboard is opened.")
    })
    public void Test3() throws Exception {
        try {
            AReport.newTest();
            AReport.reportAction("1ctgyut", true);
            AReport.reportAction("2szdtttt", true);
            AReport.reportAction("3zgh", true);
            AReport.reportAction("zdty4", true);
            AReport.NextStep();
            throw new Exception("some error");

        }
    catch (Exception ex)
    {
        AReport.finishTestUnexpected(ex.getMessage());
    }
    finally {
        AReport.finishTest();
    }
    }

    @Test
    @arp.Annotations.AutoTest(Description = "Validate search query at main page - 02072014")
    @arp.Annotations.AutoSteps({
            @arp.Annotations.AutoStep(Description = "AAA", Expected = "Login is entered"),
            @arp.Annotations.AutoStep(Description = "BFF", Expected = "Password is entered"),
            @arp.Annotations.AutoStep(Description = "CFFFFFFfff", Expected = "Button is clicked. Dashboard is opened.")
    })
    public void Test4() throws Exception {
        try {
            AReport.newTest();
            AReport.reportAction("1dfd", true);
            AReport.reportAction("2szdrgd", true);
            AReport.reportAction("3sdgsg", true);
            AReport.reportAction("4zdfgdrty", true);
            AReport.NextStep();
            AReport.reportAction("JaVazdfgdzd5", true);
            AReport.NextStep();
            AReport.reportAction("JaVafgrd5", true);

    }
    catch (Exception ex)
    {
        AReport.finishTestUnexpected(ex.getMessage());
    }
    finally {
        AReport.finishTest();
    }
    }

}


