package arp.Tests;


import arp.AReport;
import arp.CucumberArpReport;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;


/**
 * Created by grachova on 1/14/2015.
 */
public class Template {
    @BeforeClass
    public static void setUpTestClass() throws Exception {


    }

    @Before
    public void setUpTest() throws Exception {
        AReport.open(this);
    }

    @After
    public void tearDownTest() throws Exception {



    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        try {
            AReport.close();
        }
        catch (Exception ex)
        {
            throw  new Exception(ex.getMessage());
        }
    }
}
