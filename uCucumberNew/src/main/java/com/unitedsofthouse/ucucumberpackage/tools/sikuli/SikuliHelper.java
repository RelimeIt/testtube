package com.unitedsofthouse.ucucumberpackage.tools.sikuli;
import org.sikuli.api.*;
import org.sikuli.api.robot.Keyboard;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopKeyboard;
import org.sikuli.api.robot.desktop.DesktopMouse;

import java.io.File;


public class SikuliHelper {

    private String imgDirPath;
    private ScreenRegion screen;
    private double fuzzy = 0.7;
    private int timeOut = 10;
    private Mouse mouse;
    private Keyboard keyboard;
    private ScreenRegion foundRegion;

    public SikuliHelper(double fuzzy){
        screen = new DesktopScreenRegion();
        mouse = new DesktopMouse();
        keyboard = new DesktopKeyboard();
        imgDirPath = String.format("%s\\tools\\Sikuli\\img\\",System.getProperty("user.dir"));
        this.fuzzy = fuzzy;
    }

    /**
     * Get screen region of image
     * @param imageName
     * @return ScreenRegion
     */
    public ScreenRegion GetImage(String imageName){
        File file = new File(imgDirPath + imageName + ".png");
        boolean n = file.exists();
        Target imageTarget = new ImageTarget( file);
        imageTarget.setMinScore(fuzzy);
        return screen.wait(imageTarget, timeOut);
    }

    /**
     * Wait for appearing particular image on the screen
     * @param imageName
     */
    public void WaitForObject(String imageName){
        GetImage(imageName);
    }

    /**
     * Get relative screen region of image
     * @param imageName
     * @param offsetRight
     * @return
     */
    public ScreenRegion GetRelativeRegion(String imageName, int offsetRight){
        foundRegion = GetImage(imageName);
        ScreenRegion newRegion = Relative.to(foundRegion).right(offsetRight).getScreenRegion();
        return  newRegion;
    }

    /**
     * Get relative screen region of image below
     * @param imageName
     * @param offsetBelow
     * @return
     */
    public ScreenRegion GetRelativeRegionBelow(String imageName, int offsetBelow){
        foundRegion = GetImage(imageName);
        ScreenRegion newRegion = Relative.to(foundRegion).below(offsetBelow).getScreenRegion();
        return  newRegion;
    }

    /**
     * Navigate mouse to the screen location
     * @param location
     */
    public void MouseMove(ScreenLocation location){
        mouse.move(location);
    }

    /**
     * Enter a text
     * @param text
     */
    public void EnterText(String text){
        keyboard.type(text);
    }

    /**
     * Double click on center of image with particular name
     * @param imageName
     */
    public void DoubleClick(String imageName){
        mouse.doubleClick(GetImage(imageName).getCenter());
    }

    /**
     * Double click on particular screen location
     * @param location
     */
    public void DoubleClick(ScreenLocation location){
        mouse.doubleClick(location);
    }

    /**
     * Click on center of image with particular name
     * @param imageName
     */
    public void Click(String imageName){
        mouse.click(GetImage(imageName).getCenter());
    }

    /**
     * Click on particular screen location
     * @param location
     */
    public void Click(ScreenLocation location){
        mouse.click(location);
    }

}
