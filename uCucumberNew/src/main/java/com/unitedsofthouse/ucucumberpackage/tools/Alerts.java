package com.unitedsofthouse.ucucumberpackage.tools;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.concurrent.TimeUnit;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;

/**
 * Created by pasichniy on 07-Dec-15.
 */
public class Alerts {

    private Alert alert;

    /**
     * Wait while alert appear,
     * with timeOut in seconds
     *
     * @param limit in seconds
     */
    public void waitForAlert(int limit) {
        FluentWait<WebDriver> wait = new FluentWait<>(getDriver()).withTimeout(limit, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS).ignoring(NoAlertPresentException.class);
        wait.until(ExpectedConditions.alertIsPresent());
    }

    /**
     * Wait while alert appear with 10 sec timeout,
     * and switch driver to Alert window
     */
    public void waitAppearanceOfAlert() {
        waitForAlert(10);
        alert = getDriver().switchTo().alert();
    }

    /**
     * Wait while alert appear with specific timeout,
     * and switch driver to Alert window
     *
     * @param timeInSec seconds
     * @return boolean
     */
    public boolean waitAppearanceOfAlert(int timeInSec) {
        try {
            waitForAlert(timeInSec);
            alert = getDriver().switchTo().alert();
        } catch (Exception ex) {
            alert = null;
            return false;
        }
        return true;
    }

    /**
     * Accept alert
     */
    public void accept() {
        if (alert != null)
            alert.accept();
    }

    /**
     * Dismiss alert
     */
    public void dismiss() {
        if (alert != null)
            alert.dismiss();
    }

    /**
     * Get text from alert window
     *
     * @return String
     */
    public String getText() {
        if (alert != null)
            return alert.getText().trim();
        return "";
    }

    /**
     * Set text in alert window, if needed
     *
     * @param text String
     */
    public void setText(String text) {
        if (alert != null)
            alert.sendKeys(text);
    }
}
