package com.unitedsofthouse.ucucumberpackage.tools.fileparsing;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;

import java.io.*;
import java.util.ArrayList;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.getArpReportClient;
import static org.junit.Assert.assertTrue;

/**
 * Class is used to get text from different types of files
 * Created by bilokon_tanya on 2/24/2016.
 */
public class FileParsing {

    /**
     * Get content from TXT file
     * @param fullPathToFile
     * @return String Text from TXT file
     * @throws IOException
     */
    public static String readTXTFile(String fullPathToFile) throws IOException {
        FileInputStream inFile = new FileInputStream(fullPathToFile);
        byte[] str = new byte[inFile.available()];
        inFile.read(str);
        String text = new String(str);
        return text;
    }

    /**
     * Get content from PDF file
     * @param fullPathToFile
     * @return String Text from PDF file
     * @throws Exception
     */
    public static String readPDFFile(String fullPathToFile) throws Exception {
        String _FileContent = null;
        PdfReader _pdfReader = null;
        try {
            _pdfReader = new PdfReader(fullPathToFile);//get PDF file
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (_pdfReader.getNumberOfPages() == 0)
            getArpReportClient().ReportAction("PDF-file wasn't opened correctly. File-path: " + fullPathToFile, false);

        StringBuilder pdfContent = new StringBuilder();
        for (int i = 1; i <= _pdfReader.getNumberOfPages(); ++i) {
            TextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
            pdfContent.append(PdfTextExtractor.getTextFromPage(_pdfReader, i, strategy).replace("\n", " "));//get pdf file content
        }
        _FileContent = pdfContent.toString();

        return _FileContent;
    }

    /**
     * Get content from HTML file
     * @param fullPathToFile
     * @return String Text from HTML file
     * @throws IOException
     */
    public static String readHTMLFile(String fullPathToFile) throws IOException {
        String path = fullPathToFile;
        StringBuilder contentBuilder = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(path));
            String str;
            while ((str = in.readLine()) != null) {
                contentBuilder.append(str);
            }
            in.close();
        } catch (FileNotFoundException ex) {
            try {
                getArpReportClient().ReportAction("File <" + path + "> is not found!", false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        String content = contentBuilder.toString();
        return content;
    }

    /**
     * Get content from Doc file
     * @param fullPathToFile
     * @return String Text from Doc file
     * @throws Exception
     */
    public static String readDocFile(String fullPathToFile) throws Exception {
        String result = "";
        File file = new File(fullPathToFile);
        try {
            WordExtractor wordExtractor = new WordExtractor(new FileInputStream(file));
            result = wordExtractor.getText();
        } catch (FileNotFoundException ex) {
            getArpReportClient().ReportAction("File <" + fullPathToFile + "> is not found!", false);
        }
        return result;
    }

    /**
     * Get content from Excel XLSX file
     * @param fullPathToFile
     * @return String Text from Excel XLSX file
     * @throws IOException
     */
    public static String readExcelXLSXFile(String fullPathToFile) throws IOException {
        String result = "";
        String path = fullPathToFile;

        try {

            XSSFWorkbook wb = new XSSFWorkbook(path);
            XSSFSheet sheet = wb.getSheetAt(0);
            XSSFRow row;
            XSSFCell cell;


            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            int cols = 0; // No of columns
            int tmp = 0;

            // This trick ensures that we get the data properly even if it doesn't start from first few rows
            for (int i = 0; i < 10 || i < rows; i++) {
                row = sheet.getRow(i);
                if (row != null) {
                    tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                    if (tmp > cols) cols = tmp;
                }
            }

            for (int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                if (row != null) {
                    for (int c = 0; c < cols; c++) {
                        cell = row.getCell(c);
                        if (cell != null) {
                            switch (cell.getCellType()) {
                                case Cell.CELL_TYPE_NUMERIC:
                                    result += cell.getNumericCellValue() + "\t";
                                    break;
                                case Cell.CELL_TYPE_STRING:
                                    result += cell.getStringCellValue() + "\t";
                                    break;
                            }
                        }
                    }
                }
            }
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }

        return result;
    }

    /**
     * Get content from Excel XLS file
     * @param file
     * @return String Text from Excel XLS file
     * @throws IOException
     */
    public static String readExcelXLSFile(String file) throws IOException {
        //read ISIS file as text separated by tab
        ArrayList<ArrayList<String>> arList = new ArrayList<ArrayList<String>>();
        ArrayList<String> al = null;
        String thisLine;

        String result = "";

        DataInputStream myInput = new DataInputStream(new FileInputStream(file));
        while ((thisLine = myInput.readLine()) != null) {
            al = new ArrayList<String>();
            String strar[] = thisLine.split("\t");
            for (int j = 0; j < strar.length; j++) {
                String edit = strar[j].replace('\n', ' ');
                al.add(edit);
            }
            arList.add(al);
        }
        try {
            HSSFWorkbook hwb = new HSSFWorkbook();
            HSSFSheet sheet = hwb.createSheet("new sheet");

            for (int k = 0; k < arList.size(); k++) {
                ArrayList<String> ardata = (ArrayList<String>) arList.get(k);
                for (int p = 0; p < ardata.size(); p++) {
                    System.out.print(ardata.get(p));
                    result += (ardata.get(p).toString());
                }
            }
        } catch (Exception ex) {
        }
        return result;
    }
}
