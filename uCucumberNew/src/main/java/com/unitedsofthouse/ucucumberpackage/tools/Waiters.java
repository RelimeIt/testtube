package com.unitedsofthouse.ucucumberpackage.tools;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;

/**
 * Created by pasichniy on 07-Dec-15.
 */
public class Waiters {

    /**
     * Wait 1 second
     *
     * @param driver WebDriver
     */
    public static void waitConstant(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    /**
     * TimeOut in seconds
     *
     * @param driver WebDriver
     * @param timeOut  time in seconds
     */
    public static void waitSeconds(WebDriver driver, int timeOut) {
        getDriver().manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }

    /**
     * Wait appearance of WebElement
     * with tiomeOut in seconds
     *
     * @param limit in seconds
     * @param element Web Element
     */
    public static void waitAppearanceOf(int limit, WebElement element) {
        FluentWait<org.openqa.selenium.WebDriver> wait = new FluentWait<>(getDriver())
                .withTimeout(limit, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    /**
     * Wait appearance of list of WebElements
     * with tiomeOut in seconds
     *
     * @param limit in seconds
     * @param elements Web Element
     */
    public static void waitAppearanceOfAll(int limit, List<WebElement> elements) {
        FluentWait<org.openqa.selenium.WebDriver> wait = new FluentWait<>(getDriver())
                .withTimeout(limit, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    /**
     * Wait disapperance of WebElement
     * with tiomeOut in seconds
     *
     * @param limit in seconds
     * @param element Web Element
     * @param implicityWaitTimeOut
     */
    public static void waitDisappearsOf(int limit, WebElement element, int implicityWaitTimeOut) {

        waitConstant(getDriver());
        try {
            if (element != null)
                if (element.isDisplayed()) {
                    FluentWait<WebDriver> wait = new FluentWait<>(getDriver())
                            .withTimeout(limit, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS);
                    wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
                }
        } catch (NoSuchElementException ex) {
        }

        waitSeconds(getDriver(), implicityWaitTimeOut);
    }

    /**
     * Wait while some text disappear from WebElement,
     * with timeOut in seconds
     *
     * @param limit in seconds
     * @param element WebElement
     * @param text text, that must disappear
     */
    public static void waitForTextDisappear(int limit, WebElement element, String text) {
        FluentWait<WebDriver> wait = new FluentWait<>(getDriver()).withTimeout(limit, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElement(element, text)));
    }

    /**
     * Wait for special condition
     * with tiomeOut in seconds
     *
     * @param limit in seconds
     * @param expectedCondition ExpectedCondition
     */
    public static void waitForCustomCondition(int limit, ExpectedCondition<Boolean> expectedCondition) {
        FluentWait<org.openqa.selenium.WebDriver> wait = new FluentWait<>(getDriver())
                .withTimeout(limit, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class)
                .ignoring(TimeoutException.class);
        wait.until(expectedCondition);
    }

    /**
     * Executing until condition is true.
     *
     * @param times int 1 = around 1 second
     * @param condition boolean
     */
    public static void waitWhileConditionIsTrue(int times, boolean condition) {
        for (int i = 0; i < times; i++) {
            if (condition) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else return;
        }
    }

    /**
     * Timeout after some action
     *
     * @param limit in seconds
     * @param consumer Consumer
     * @param expCond ExpectedCondition<Boolean>
     * @param element Web Element
     */
    public static void waitAfterAction(int limit, Consumer<? super Element> consumer, ExpectedCondition<Boolean> expCond, Element element) {

        FluentWait<WebDriver> wait = new FluentWait<>(getDriver()).withTimeout(limit, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS).ignoring(TimeoutException.class)
                .ignoring(NoSuchElementException.class, StaleElementReferenceException.class);
        consumer.accept(element);
        wait.until(expCond);
    }
}
