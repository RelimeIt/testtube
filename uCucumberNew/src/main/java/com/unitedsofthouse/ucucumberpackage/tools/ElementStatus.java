package com.unitedsofthouse.ucucumberpackage.tools;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;

/**
 * Created by pasichniy on 07-Dec-15.
 */
public class ElementStatus {

//    /**
//     * Check is web element required
//     *
//     * @param element
//     * @return boolean
//     */
//    public static boolean isWebElementRequired(WebElement element) {
//        try {
//            return element.findElements(By.xpath(".//font[contains(.,'*')]")).size() > 0;
//        } catch (Exception ex) {
//            return false;
//        }
//    }

    /**
     * Check is web element disabled
     *
     * @param element web element
     * @return boolean
     */
    public static boolean isWebElementDisabled(WebElement element) {
        try {
            return (element.getAttribute("disabled") != null);
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Check is web element displayed
     *
     * @param webElement
     * @return boolean
     */
    public static boolean isWebElementDisplayed(final WebElement webElement) {
        try {
            return (new WebDriverWait(getDriver(), 1))
                    .until((ExpectedCondition<Boolean>) p -> webElement.isDisplayed());
        } catch (WebDriverException ex) {
            return false;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Check is html element displayed
     *
     * @param element uCucumberPackage Element
     * @return boolean
     */
    public static boolean isHtmlElementDisplayed(final Element element) {
        return isWebElementDisplayed(element.getWrappedElement());
    }


}
