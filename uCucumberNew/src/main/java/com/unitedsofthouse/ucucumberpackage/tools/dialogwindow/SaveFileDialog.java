package com.unitedsofthouse.ucucumberpackage.tools.dialogwindow;

import com.unitedsofthouse.ucucumberpackage.tools.sikuli.SikuliHelper;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.sikuli.api.ScreenRegion;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;

/**
 * SaveFileDialog work with save file dialog window
 */
public class SaveFileDialog {

    /**
     * work with Save As dialog window
     * @param name path to save file with file name
     * @param secondsCount wait time out
     */
    public static void saveAsDialog(String name, int secondsCount) throws Exception {
        Capabilities capabilities = ((RemoteWebDriver) getDriver()).getCapabilities();
        String browser = capabilities.getBrowserName();
        String version = capabilities.getVersion();
        switch (browser) {
            case "internet explorer":
                saveInIE(name, secondsCount);
                break;
            case "firefox":
                saveInFF(name, secondsCount);
                break;
            case "chrome":
                saveInCH(name, secondsCount);
                break;
            default:
                throw new Exception("This browser does not describe.");
        }
    }

    /**
     * Check if alert pop-up window is present
     * try to switch to save alert and return result
     * @return Boolean true - if alert is present, false - if alert not present
     */
    private static Boolean isAlertPresent() {
        try {
            getDriver().switchTo().alert();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Method is used to download and save file from Chrome browser
     * @param name path to save file with file name
     * @param secondsCount wait time out
     */
    private static void saveInCH(String name, Integer secondsCount) throws Exception {
        String currentPath = System.getProperty("user.dir");
        Process save = Runtime.getRuntime().exec(new String[]{currentPath + "\\tools\\saveCH.exe", secondsCount.toString(), name});//launch save script
        save.waitFor();
        Path path = Paths.get(name);
        for (int i = 0; i < secondsCount; i++) {
            Thread.sleep(1000);
            if (new File(name).isFile())
                if (Files.size(path) != 0)//wait until file downloaded
                    break;
        }
    }

    /**
     * Method is used to download and save file from FireFox browser
     * @param name path to save file with file name
     * @param secondsCount wait time out
     */
    private static void saveInFF(String name, Integer secondsCount) throws Exception {
        String currentPath = System.getProperty("user.dir");
        Process save = Runtime.getRuntime().exec(new String[]{currentPath + "\\tools\\saveFF.exe", secondsCount.toString(), name});//launch save script
        save.waitFor();
        File file = new File(name);
        Path path = Paths.get(name);//
        for (int i = 0; i < secondsCount; i++) {
            Thread.sleep(1000);
            if (Files.size(path) != 0)//wait until file downloaded
                break;
        }
    }

    /**
     * ckeck is IE download PopUp appears
     * @return true - if alert is present, false - if alert not present
     */
    private static boolean isIEDownloadPopUpAppears() {
        SikuliHelper sikuli = new SikuliHelper(0.7);
        ScreenRegion img3 = sikuli.GetImage("IE9SaveWhite");
        if (img3 != null)
            return true;
        else return false;
    }

    /**
     * Method is used to download and save file from Internet Explorer browser
     * @param name path to save file with file name
     * @param secondsCount wait time out
     */
    private static void saveInIE(String name, Integer secondsCount) throws Exception {
        String currentPath = System.getProperty("user.dir");
        String folder = System.getProperty("user.home") + "\\Downloads";
        File directory = new File(folder);
        File[] listWithFiles = directory.listFiles();//get files from default Downloads directory before start export
        assert listWithFiles != null;
        int k = 0;
        do {
            k++;
        } while (!isIEDownloadPopUpAppears() && k < secondsCount / 10);//wait until save mini window appears
        try {
            Process save = Runtime.getRuntime().exec(new String[]{currentPath + "\\tools\\saveIE.exe", secondsCount.toString(), name, "param1"});//launch save script
            save.waitFor();
        } catch (Exception ex) {
            throw new Exception("Can not save file, check AutoIT script.");
        }
        File file;
        File[] listFilesAfterExport;
        int counter = 0;
        do {
            if (counter == secondsCount) {
                throw new Exception("File was not downloaded after timeout");
            }
            Thread.sleep(1000);
            counter++;
            listFilesAfterExport = directory.listFiles();//get files from default Downloads directory after start export
            assert listFilesAfterExport != null;
            file = listFilesAfterExport[0];
            for (File f : listFilesAfterExport) {
                if (f.lastModified() > file.lastModified()) {
                    file = f;
                }
            }
        } while (file.getName().contains(".partial"));//wait until file downloaded
        if (listWithFiles.length >= listFilesAfterExport.length) {//check if count files is changed
            throw new Exception("File not appears in folder 'Downloads'");
        }
        try {
            Path to = Paths.get(name);//path to our download directory
            Files.move(file.toPath(), to);//move and rename file to our download directory
        } catch (Exception ex) {
            throw new Exception("Can not move file " + name + " !!! Exception: <b>" + ex.getMessage() + "<//b>");
        }
        for (int i = 0; i < secondsCount; i++) {
            Thread.sleep(1000);
            if (new File(name).isFile())//wait file
                break;
        }
        // Need to write autoIt script:
        Process close = Runtime.getRuntime().exec(new String[]{currentPath + "\\tools\\CloseSaveDialogIE.exe"});//close save dialog ()
        close.waitFor();
    }
}
