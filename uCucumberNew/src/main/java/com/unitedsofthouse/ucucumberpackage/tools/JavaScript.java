package com.unitedsofthouse.ucucumberpackage.tools;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;

/**
 * JavaScript class use Java Script to work with web element
 */
public class JavaScript {
    /**
     * @param element web element
     * @param attributeName attribute name to find element
     * @return String with web element value
     */
    public static String getValueByJS(WebElement element, String attributeName) {
        if (!element.getAttribute(attributeName).isEmpty() && element.getAttribute(attributeName).equals("")) {
            return ((JavascriptExecutor) getDriver()).executeScript(String.format("return document.getElementById('%1$s').value", element.getAttribute(attributeName))).toString();
        } else
            return null;
    }

    /**
     * @param element web element
     * @param valueToSet value to set into web element
     */
    public static void setValueWithJavaScript(WebElement element, String valueToSet) {
        ((JavascriptExecutor) getDriver()).executeScript("$(arguments[0]).val(arguments[1]);", element.findElement(By.xpath("./.")), valueToSet); //if 'element' is a proxy class
    }

    /**
     * @param element web element
     * @param valueToSet value to set into web element
     */
    public static void setValueToFrame(WebElement element, String valueToSet) {
        ((JavascriptExecutor) getDriver()).executeScript(String.format("$(arguments[0]).find('iframe').contents().find('body').text('%1$s');", valueToSet), element.findElement(By.xpath("./.")));
    }

    /**
     * @param element web element
     * @return String with value from web element
     */
    public static String getValueFromFrame(WebElement element) {
        return ((String) ((JavascriptExecutor) getDriver()).executeScript("return $(arguments[0]).find('iframe').contents().find('body').text();", element)).trim();
    }

    /**
     * run JS event for web element
     * @param element web element
     * @param attributeName attribute name
     */
    public static void runJSEvent(WebElement element, String attributeName) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0]", element.getAttribute(attributeName));
    }

    /**
     * click and wait condition for approval result of click
     * @param element web element
     * @param seconds time (seconds) for waiting custom conditions
     * @param expectedCondition condition for approval result of click
     */
    public static void clickWithWaiting(WebElement element, int seconds, ExpectedCondition<Boolean> expectedCondition) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].clickOnElement();", element);
        Waiters.waitForCustomCondition(seconds, expectedCondition);
    }

    /**
     * click and wait condition for approval result of click and ready state
     * @param element web element
     * @param seconds time (seconds) for waiting custom conditions
     * @param expectedCondition condition for approval result of click
     */
    public static void clickWithWaitingReadyState(WebElement element, int seconds, ExpectedCondition<Boolean> expectedCondition) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].clickOnElement();", element);
        Waiters.waitForCustomCondition(seconds, p -> ((JavascriptExecutor) p).executeScript("return document.readyState;").equals("complete"));
        Waiters.waitForCustomCondition(seconds, expectedCondition);
    }

    /**
     * Find element by JQuery asynchronously
     * @param cssLocator css locator to find web element
     * @return WebElement found web element
     */
    public static WebElement findElementByJQueryAsync(String cssLocator) {
        try {
            String toRun = ("var tag = $('" + cssLocator.replaceAll("'", "\"").replaceAll(":eq(0)", "") + "').first().prop('tagName');" +
                    "if (tag == undefined) { throw 'Element not found!';}" +
                    "return $('" + cssLocator.replaceAll("'", "\"").replaceAll(":eq(0)", "") + "').first()[0];").replaceAll("\r\n", "");
            return (WebElement) (((JavascriptExecutor) getDriver()).executeAsyncScript(toRun));
        } catch (Exception ex) {
        }
        return null;
    }

    /**
     * Find element by JQuery synchronously
     * @param cssLocator css locator to find web element
     * @return WebElement found web element
     */
    public static WebElement findElementByJQuerySync(String cssLocator) {
        try {
            String toRun = ("var tag = $('" + cssLocator.replaceAll("'", "\"").replaceAll(":eq(0)", "") + "').first().prop('tagName');" +
                    "if (tag == undefined) { throw 'Element not found!';}" +
                    "return $('" + cssLocator.replaceAll("'", "\"").replaceAll(":eq(0)", "") + "').first()[0];").replaceAll("\r\n", "");
            return (WebElement) (((JavascriptExecutor) getDriver()).executeScript(toRun));
        } catch (Exception ex) {
        }
        return null;
    }
}
