package com.unitedsofthouse.ucucumberpackage.tools;

import com.machinepublishers.jbrowserdriver.JBrowserDriver;
import com.machinepublishers.jbrowserdriver.Settings;
import com.machinepublishers.jbrowserdriver.Timezone;
import cucumber.api.Scenario;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WebCucDriver {
    private static WebDriver driver;
    private static File file;
    private static DesiredCapabilities capabilities;

    //settings for re-setUpWebdriver
    private static FirefoxProfile firefoxProfile;
    private static String browser, proxyHost, proxyPort;
    private static Boolean withSaveDialog;

    /**
     * Get Web Cucumber driver
     * re setup it if null
     *
     * @return Web driver
     */
    public static WebDriver getDriver() {
        if (driver == null)
            setWebDriver(browser, proxyHost, proxyPort, capabilities, firefoxProfile, withSaveDialog);
        return driver;
    }

    /**
     * method Re-setup Web driver with last saved parameters;
     *
     * @param _browser        saved browser name
     * @param _proxyHost      save proxy Host
     * @param _proxyPort      saved proxy Port
     * @param _capabilities   saved DesiredCapabilities
     * @param _firefoxProfile saved Firefox profile
     * @param _withSaveDialog saved parameter is save file dialog present
     */
    private static void setWebDriver(String _browser, String _proxyHost, String _proxyPort, DesiredCapabilities _capabilities, FirefoxProfile _firefoxProfile, Boolean _withSaveDialog) {
        if (_proxyHost == null && _capabilities == null && _browser != null)
            setWebdriver(_browser, _withSaveDialog);
        if (_proxyHost == null && _capabilities != null)
            setWebdriver(_browser, _capabilities, _firefoxProfile);
        if (_proxyHost != null)
            setWebdriver(_browser, _proxyHost, _proxyPort, _withSaveDialog);
        else setWebdriver("CH", false);
    }

    /**
     * Configure web driver with proxy
     *
     * @param _browser        Broser (
     *                        IE - Internet Explorer,
     *                        CH - Google Chrome,
     *                        FF - Mozilla Firefox,
     *                        SF - Safari)
     * @param _proxyHost      url or IP of proxy server
     * @param _proxyPort      port number
     * @param _withSaveDialog is Save dialog use
     */
    public static void setWebdriver(String _browser, String _proxyHost, String _proxyPort, Boolean _withSaveDialog) {
        browser = _browser;
        proxyHost = _proxyHost;
        proxyPort = _proxyPort;
        withSaveDialog = _withSaveDialog;
        String OS = System.getProperty("os.name").toLowerCase();
        boolean isUnix = OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0;
        Proxy proxy = new Proxy();
        String proxyAdress = _proxyHost + ":" + _proxyPort;
        proxy.setHttpProxy(proxyAdress)
                .setFtpProxy(proxyAdress)
                .setSslProxy(proxyAdress);

        switch (_browser.toUpperCase().trim()) {
            case "IE":
                file = new File("tools/IEDriverServer.exe");
                capabilities = DesiredCapabilities.internetExplorer();
                System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
                capabilities.setCapability(CapabilityType.PROXY, proxy);
                capabilities.setCapability("nativeEvents", false);
                capabilities.setJavascriptEnabled(true);
                driver = new InternetExplorerDriver(capabilities);
                break;
            case "CH":
                capabilities = DesiredCapabilities.chrome();
                if (_withSaveDialog) {
                    Map<String, Object> prefs = new HashMap<>();
                    prefs.put("download.prompt_for_download", true);
                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("prefs", prefs);
                    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                }
                if (isUnix) {
                    file = new File("tools/chromedriver");
                } else {
                    file = new File("tools/chromedriver.exe");
                }
                System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
                capabilities.setCapability("nativeEvents", false);
                capabilities.setCapability(CapabilityType.PROXY, proxy);
                driver = new ChromeDriver(capabilities);
                break;
            case "FF":
                FirefoxProfile firefoxProfile = new FirefoxProfile();
                firefoxProfile.setPreference("network.proxy.type", 1);
                firefoxProfile.setPreference("network.proxy.http", _proxyHost);
                firefoxProfile.setPreference("network.proxy.http_port", Integer.parseInt(_proxyPort));
                firefoxProfile.setPreference("security.enable_java", true);
                firefoxProfile.setPreference("plugin.state.java", 2);
                firefoxProfile.setPreference("extensions.blocklist.enabled", false);
                firefoxProfile.setPreference("network.proxy.ssl", _proxyHost);
                firefoxProfile.setPreference("network.proxy.ssl_port", Integer.parseInt(_proxyPort));

                if (_withSaveDialog) {
                    firefoxProfile.setPreference("browser.download.useDownloadDir", false);
                    firefoxProfile.setPreference("browser.helperApps.alwaysAsk.force", true);
                }
                driver = new FirefoxDriver(firefoxProfile);
                break;
            case "SF":
                driver = new SafariDriver();
                break;
            case "JB":
                capabilities = new DesiredCapabilities("jbrowserdriver", "1", Platform.ANY);
                capabilities.merge(Settings.builder().buildCapabilities());
                driver = new JBrowserDriver(capabilities);
                break;
            default:
                driver = new FirefoxDriver();
                break;
        }
    }

    /**
     * Configure web driver
     *
     * @param _browser        Broser (
     *                        IE - Internet Explorer,
     *                        CH - Google Chrome,
     *                        FF - Mozilla Firefox,
     *                        SF - Safari)
     * @param _withSaveDialog is Save dialog use
     */
    public static void setWebdriver(String _browser, Boolean _withSaveDialog) {
        browser = _browser;
        withSaveDialog = _withSaveDialog;
        String OS = System.getProperty("os.name").toLowerCase();
        boolean isUnix = OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0;
        switch (_browser.toUpperCase().trim()) {
            case "IE":
                file = new File("tools/IEDriverServer.exe");
                capabilities = DesiredCapabilities.internetExplorer();
                System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
                capabilities.setCapability("nativeEvents", false);
                capabilities.setJavascriptEnabled(true);
                driver = new InternetExplorerDriver(capabilities);
                break;
            case "CH":
                capabilities = DesiredCapabilities.chrome();
                if (_withSaveDialog) {
                    Map<String, Object> prefs = new HashMap<>();
                    prefs.put("download.prompt_for_download", true);
                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("prefs", prefs);
                    capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                }
                if (isUnix) {
                    file = new File("tools/chromedriver");
                } else {
                    file = new File("tools/chromedriver.exe");
                }
                System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
                capabilities.setCapability("nativeEvents", false);
                driver = new ChromeDriver(capabilities);
                break;
            case "FF":
                FirefoxProfile firefoxProfile = new FirefoxProfile();
                firefoxProfile.setPreference("security.enable_java", true);
                firefoxProfile.setPreference("plugin.state.java", 2);
                firefoxProfile.setPreference("extensions.blocklist.enabled", false);
                if (_withSaveDialog) {
                    firefoxProfile.setPreference("browser.download.useDownloadDir", false);
                    firefoxProfile.setPreference("browser.helperApps.alwaysAsk.force", true);
                }
                driver = new FirefoxDriver(firefoxProfile);
                break;
            case "SF":
                driver = new SafariDriver();
                break;
            case "PD":
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setJavascriptEnabled(true);
                caps.setCapability("takesScreenshot", false);
                caps.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent", "My User Agent - Chrome");
                caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[]{
                        "--web-security=false",
                        "--ssl-protocol=any",
                        "--ignore-ssl-errors=true",
                        "--webdriver-loglevel=DEBUG"
                });
                caps.setCapability(
                        PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
                        "tools\\phantomjs.exe"
                );
                driver = new PhantomJSDriver(caps);
                break;
            case "JB":
                capabilities = new DesiredCapabilities("jbrowserdriver", "1", Platform.ANY);
                capabilities.merge(Settings.builder().buildCapabilities());
                driver = new JBrowserDriver(capabilities);
                break;
            default:
                driver = new FirefoxDriver();
                break;
        }
    }

    /**
     * Configure web driver with capabilities and firefoxProfile (if browser - FF)
     *
     * @param _browser        Broser (
     *                        IE - Internet Explorer,
     *                        CH - Google Chrome,
     *                        FF - Mozilla Firefox,
     *                        SF - Safari)
     * @param _capabilities   DesiredCapabilities settings
     * @param _firefoxProfile profile for Firefox only and apply if _capabilities == null
     */
    public static void setWebdriver(String _browser, DesiredCapabilities _capabilities, FirefoxProfile _firefoxProfile) {
        String OS = System.getProperty("os.name").toLowerCase();
        boolean isUnix = OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0;
        browser = _browser;
        capabilities = _capabilities;
        firefoxProfile = _firefoxProfile;
        switch (_browser.toUpperCase().trim()) {
            case "IE":
                file = new File("tools/IEDriverServer.exe");
                System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
                driver = new InternetExplorerDriver(_capabilities);
                break;
            case "CH":
                if (isUnix) {
                    file = new File("tools/chromedriver");
                } else {
                    file = new File("tools/chromedriver.exe");
                }
                System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
                driver = new ChromeDriver(_capabilities);
                break;
            case "FF":
                if (_capabilities != null) {
                    driver = new FirefoxDriver(_capabilities);
                } else if (_firefoxProfile != null) {
                    driver = new FirefoxDriver(_firefoxProfile);
                }
            case "SF":
                if (_capabilities != null) {
                    driver = new SafariDriver(_capabilities);
                }
                break;
            case "JB":
                capabilities = new DesiredCapabilities("jbrowserdriver", "1", Platform.ANY);
                capabilities.merge(Settings.builder().buildCapabilities());
                driver = new JBrowserDriver(capabilities);
                break;
            default:
                driver = new FirefoxDriver(_capabilities);
                break;
        }
    }

    /**
     * Takes screenshot
     *
     * @param scenario Scenario
     * @throws AWTException
     * @throws IOException
     */
    public static void takeScreenshot(Scenario scenario) throws AWTException, IOException {
        try {
            byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        }
    }
}
