package com.unitedsofthouse.ucucumberpackage.tools.dialogwindow;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;
/**
 * OpenFileDialog class work with open file dialog window
 */
public class OpenFileDialog {

    /**
     * create open file dialog window
     * @param name path to file with file name
     * @param secondsCount wait time out
     */
    public static void OpenFile(String name, int secondsCount) throws Exception {
        Capabilities capabilities = ((RemoteWebDriver) getDriver()).getCapabilities();
        String browser = capabilities.getBrowserName();
        switch (browser) {
            case "internet explorer":
                break;
            case "firefox":
                OpenInFF(name, secondsCount);
                break;
            case "chrome":
                break;
            default:
                throw new Exception("This browser does not describe.");
        }
    }

    /**
     * work with open file dialog window in firefox browser
     * @param name path to file with file name
     * @param secondsCount wait time out
     */
    private static void OpenInFF(String name, Integer secondsCount) throws Exception {
        Process save = Runtime.getRuntime().exec(new String[]{System.getProperty("user.dir") + "\\tools\\OpenFileFF.exe", secondsCount.toString(), name});//launch save script
        save.waitFor();
        Path path = Paths.get(name);
        for (int i = 0; i < secondsCount; i++) {
            Thread.sleep(1000);
            if (Files.size(path) != 0)//wait until file downloaded
                break;
        }
    }
}
