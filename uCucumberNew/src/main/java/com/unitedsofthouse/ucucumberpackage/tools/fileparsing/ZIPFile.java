package com.unitedsofthouse.ucucumberpackage.tools.fileparsing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Class is used to extract files from ZIP archive
 * Created by bilokon_tanya on 2/24/2016.
 */
public class ZIPFile {

    List<String> _listNames = new ArrayList<>();
    List<String> _listPaths = new ArrayList<>();

    /**
     * Method unzip archive.
     * Remember file names and paths
     *
     * @param zipPath      - path to zip file
     * @param outputFolder - path to folder in which to unpack the archive
     * @throws Exception
     */
    private void getFileListFromZip(String zipPath, String outputFolder) throws Exception {
        _listNames.clear();
        _listPaths.clear();
        byte[] buffer = new byte[1024];
        try {
            //create output directory
            File folder = new File(outputFolder);
            folder.mkdir();
            //get the zip file content
            ZipInputStream zis = new ZipInputStream(new FileInputStream(zipPath));
            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {
                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);
                _listNames.add(fileName);
                _listPaths.add(newFile.getAbsolutePath());//remember file names and paths
                new File(newFile.getParent()).mkdirs();//create all directories
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {//unzip
                    fos.write(buffer, 0, len);
                }
                fos.close();
                ze = zis.getNextEntry();
            }
            zis.closeEntry();
            zis.close();
//            System.out.println("Done");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Get list paths to Extract Files
     *
     * @param zipPath - path to zip file
     * @return list paths
     * @throws Exception
     */
    public List<String> list_PathsOfExtractFiles(String zipPath) throws Exception {
        String outputFolder = zipPath.replace(".zip", "").replace(".brd", "").replace(".bpd", "");
//        if (_listPaths.size() == 0)
        getFileListFromZip(zipPath, outputFolder);
        return _listPaths;
    }

    /**
     * Get list names of Extract Files
     *
     * @param zipPath - path to zip file
     * @return list names
     * @throws Exception
     */
    public List<String> list_FullNamesOfExtractFiles(String zipPath) throws Exception {
        String outputFolder = zipPath.replace(".zip", "").replace(".brd", "").replace(".bpd", "");
//        if (_listNames.size() == 0)
        getFileListFromZip(zipPath, outputFolder);
        return _listNames;
    }
}
