package com.unitedsofthouse.ucucumberpackage.tools.dialogwindow;

import javax.mail.*;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.SubjectTerm;
import java.io.*;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
/** MailClient class - work with mail agent */
public class MailClient {
    //substitute your isp's POP3 server
    String host = "imap.gmail.com";
    //substitute your username and password to access e-mail
    String user = "";
    String password = "";

    protected Message message;
    protected String mailFilePath = "";
    protected String mailBody = "";
    protected MimeMultipart originalMail;
    public String attachmentFilePath;
    int port = 993;

    //Get a session. Use Properties object.
    Session session;
    Store store;

    /**
     * create mail agent with all settings
     * @param _host     host
     * @param _port     port
     * @param _user     userName
     * @param _password userPassword
     */
    public MailClient(String _host, int _port, String _user, String _password) throws MessagingException {
        this.host = _host;
        this.user = _user;
        this.password = _password;
        this.port = _port;
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
        session = Session.getDefaultInstance(props, null);
        //session = Session.getInstance(props);
        store = session.getStore();
        store.connect(host, port, user, password);
    }

    /**
     * create mail agent without host and port
     * @param userName     Name
     * @param userPassword Password
     */
    public MailClient(String userName, String userPassword) throws MessagingException {
        this.user = userName;
        this.password = userPassword;
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
        session = Session.getDefaultInstance(props, null);
        //session = Session.getInstance(props);
        store = session.getStore();
        store.connect(host, port, user, password);
    }

    /**
     * save attachments from mail to file
     * @param path path to saved file
     */
    private void saveAttachmentToFile(String path) throws MessagingException, FileNotFoundException, IOException {
        Multipart multi = (Multipart) originalMail;
        for (int i = 0; i < multi.getCount(); i++) {
            MimeBodyPart part = (MimeBodyPart) multi.getBodyPart(i);
            if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                FileOutputStream out = new FileOutputStream(path);
                InputStream in = part.getInputStream();
                byte[] buf = new byte[4096];
                int reader;
                while ((reader = in.read(buf)) != -1) {
                    out.write(buf, 0, reader);
                }
                out.close();
            }
        }
    }

    /**
     * wait to deliver mail message
     * @param subject       subject of expected mail
     * @param fromDate      date of start mail sending
     * @param toDate        date of end mail sending
     * @param secondsCount  time to wait
     */
    public void waitToDeliver(String subject, Date fromDate, Date toDate, int secondsCount) throws MessagingException {
        boolean isSubmitted = false;
        long startTime = System.currentTimeMillis();
        long currentTime;
        int messageCount = 0;
        Folder folderInbox = store.getFolder("INBOX");
        do {
            folderInbox.open(Folder.READ_WRITE);
            // Search for e-mails by some subject substring and get some message references
            Message[] found = folderInbox.search(new SubjectTerm(subject));
            if (messageCount < found.length) {
                for (Message mess : found) {
                    // Get some headers
                    Date sentDate = mess.getSentDate();
                    if (mess.getSubject().equalsIgnoreCase(subject) && sentDate.after(fromDate) && sentDate.before(toDate)) {
                        try {
                            // All encrypted file emails should be Multipart Messages
                            Object content = mess.getContent();
                            if (content instanceof MimeMultipart) {
                                /** Get 2nd part of the message (multi-parts are zero based) should be
                                    an attached file. Write to a temporary area. */
                                MimeMultipart mimeMulti = (MimeMultipart) content;
                                originalMail = mimeMulti;
                                MimeBodyPart mimePart = (MimeBodyPart) mimeMulti.getBodyPart(0);
                                File tempFile = File.createTempFile("file", ".htm");
                                FileOutputStream out = new FileOutputStream(tempFile);
                                mimePart.getDataHandler().writeTo(out);
                                this.mailFilePath = tempFile.toString();
                                out.close();
                                // Message was processed successfully, delete it.
                                mess.setFlag(Flags.Flag.DELETED, true);
                                this.message = mess;
                                folderInbox.close(true);
                                store.close();
                                isSubmitted = true;
                                break;
                            }
                        } catch (Exception e) {
                            System.out.println("Error occurred: " + e);
                        }
                    }
                }
            }
            currentTime = System.currentTimeMillis();
            toDate = new Date();
            if (!isSubmitted) {
                folderInbox.close(true);
                messageCount = found.length;
            }
        } while (!isSubmitted && currentTime - startTime < secondsCount * 1000);
        if (this.message == null)
            System.out.println("Email were not isSubmitted id " + (currentTime - startTime) / 1000000 + " seconds");
    }

    /**
     * wait to deliver mail message
     * @param subject            subject of expected mail
     * @param fromDate           date of start mail sending
     * @param toDate             date of end mail sending
     * @param secondsCount      time to wait
     * @param attachmentFileType type of attached file
     */
    public void waitToDeliver(String subject, Date fromDate, Date toDate, int secondsCount, String attachmentFileType) throws MessagingException {
        boolean isSubmitted = false;
        long startTime = System.currentTimeMillis();
        long currentTime;
        int messageCount = 0;
        Folder inboxFolder = store.getFolder("INBOX");
        do {
            inboxFolder.open(Folder.READ_WRITE);
            //Search for e-mails by some subject substring and get some message references
            Message[] found = inboxFolder.search(new SubjectTerm(subject));
            if (messageCount < found.length) {
                for (Message mess : found) {
                    // Get some headers
                    Date sentDate = mess.getSentDate();
                    if (mess.getSubject().equalsIgnoreCase(subject) && sentDate.after(fromDate) && sentDate.before(toDate)) {
                        try {
                            // All "Encrypted File" emails should be Multipart Messages
                            Object content = mess.getContent();
                            if (content instanceof MimeMultipart) {
                                /** Get 2nd part of the message (multi-parts are zero based) should be
                                    an attached file. Write to a temporary area. */
                                MimeMultipart mimeMulti = (MimeMultipart) content;
                                originalMail = mimeMulti;
                                attachmentFilePath = generateDownloadedFileName(attachmentFileType);
                                saveAttachmentToFile(attachmentFilePath);
                                MimeBodyPart mimePart = (MimeBodyPart) mimeMulti.getBodyPart(0);
                                File tempFile = File.createTempFile("file", ".htm");
                                FileOutputStream out = new FileOutputStream(tempFile);
                                mimePart.getDataHandler().writeTo(out);
                                this.mailFilePath = tempFile.toString();
                                out.close();
                                // Message was processed successfully, delete it.
                                mess.setFlag(Flags.Flag.DELETED, true);
                                this.message = mess;
                                inboxFolder.close(true);
                                store.close();
                                isSubmitted = true;
                                break;
                            }
                        } catch (Exception e) {
                            System.out.println("Error occurred: " + e);
                        }
                    }
                }
            }
            currentTime = System.currentTimeMillis();
            toDate = new Date();
            if (!isSubmitted) {
                inboxFolder.close(true);
                messageCount = found.length;
            }
        } while (!isSubmitted && currentTime - startTime < secondsCount * 1000);
        if (this.message == null)
            System.out.println("Email were not isSubmitted id " + (currentTime - startTime) / 1000000 + " seconds");

    }

    /**
     * Get content of mail message
     * @return String with body of received mail message
     */
    public String getMailBody() throws IOException {
        File file = new File(mailFilePath);
        mailBody = com.google.common.io.Files.toString(file, Charset.defaultCharset());
        return mailBody;
    }

    /**
     * Get message
     * @return Message of received mail
     */
    public Message getMessage() {
        return message;
    }

    /**
     * @return String - path to file with mail copy
     */
    public String getMailFilePath() {
        return mailFilePath;
    }

    /**
     * Get message from mail Inbox using filter by message subject
     * @param subject subject of expected message
     * @return Message String with first message from mail box by subject
     */
    public Message getMessageBySubject(String subject) throws MessagingException {
        Folder inboxFolder = store.getFolder("INBOX");
        inboxFolder.open(Folder.READ_WRITE);
        //Search for e-mails by some substring and get some message references
        Message[] found = inboxFolder.search(new SubjectTerm(subject));
        for (Message mess : found) {
            if (mess.getSubject().equalsIgnoreCase(subject)) {
                inboxFolder.close(true);
                store.close();
                return mess;
            }
        }
        return null;
    }

    /**
     * Get message from mail Inbox using filter by message subject and sending date
     * @param subject
     * @param sendingDate
     * @return Message first message from mail box by subject and sending Date
     */
    public Message getMessageBySubjectANDDate(String subject, Date sendingDate) throws MessagingException {
        Folder inboxFolder = store.getFolder("INBOX");
        inboxFolder.open(Folder.READ_WRITE);
        //Search for e-mails by some subject substring and get some message references
        Message[] found = inboxFolder.search(new SubjectTerm(subject));
        for (Message mess : found) {
            if (mess.getSubject().equalsIgnoreCase(subject) && mess.getSentDate().after(sendingDate)) {
                inboxFolder.close(true);
                store.close();
                // "true" actually deletes flagged messages from folder
                return mess;
            }
        }
        return null;
    }

    /**
     * Get message from mail Inbox using filter by message subject, from date and to date
     * @param subject  subject of expected mail
     * @param fromDate date of start mail sending
     * @param toDate   date of end mail sending
     * @return Message first message from mail box by subject and sending Date
     */
    public Message getMessageBySubjectANDTime(String subject, Date fromDate, Date toDate) throws MessagingException {
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);
        //Search for e-mails by some subject substring and get some message references
        Message[] found = inbox.search(new SubjectTerm(subject));
        for (Message mess : found) {
            if (mess.getSubject().equalsIgnoreCase(subject) && toDate.after(fromDate) && fromDate.before(toDate)) {
                inbox.close(true);
                store.close();
                // "true" actually deletes flagged messages from folder
                return mess;
            }
        }
        return null;
    }

    /**
     * Generate name(path) for downloaded file
     * @param fileType file type
     * @return String with file directory
     */
    private String generateDownloadedFileName(String fileType) throws IOException {
        String fileName = UUID.randomUUID().toString();
        File filePath = new File(new File(System.getProperty("user.filePath") + "\\ExternalResources"), "DownloadedFiles");
        if (!filePath.exists() && !filePath.mkdirs()) {
            throw new IOException("Unable to create " + filePath.getAbsolutePath());
        }
        String fileDirectory = filePath.getAbsolutePath();
        return fileDirectory + "\\" + fileName + "." + fileType;
    }
}
