//package com.unitedsofthouse.ucucumberpackage.tools.dialogwindow;
//
//import org.openqa.selenium.Capabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;
//
//import java.io.File;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.Arrays;
//
//import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;
///**
// * Created by pasichniy on 07-Dec-15.
// */
//public class SaveFile {
//
//    /**
//     * Check browsers name and launch different 'Save' methods for different browsers
//     *
//     * @param FileName
//     * @param waitTimeOut
//     * @throws Exception
//     */
//    public static void saveAsDialog(String FileName, int waitTimeOut) throws Exception {
//        Capabilities capabilities = ((RemoteWebDriver) getDriver()).getCapabilities();
//        String browserName = capabilities.getBrowserName();
//        switch (browserName) {
//            case "internet explorer":
//                SaveIE(FileName, waitTimeOut);
//                break;
//            case "chrome":
//                SaveCH(FileName, waitTimeOut);
//                break;
//            case "firefox":
//                SaveFF(FileName, waitTimeOut);
//                break;
//            default:
//                throw new Exception("Not ready.");
//        }
//    }
//
//    /**
//     * Save file in Internet Explorer browser
//     * run AutoIt script (individual for each project)
//     * place AutoIt script exe file to 'project folder'/tools
//     *
//     * @param fileName
//     * @param waitTimeOut
//     * @throws Exception
//     */
//    private static void SaveIE(String fileName, Integer waitTimeOut) throws Exception {
//        String folder = System.getProperty("user.home") + "\\Downloads";
//        File dirToDownload = new File(folder);
//        File[] listFiles = dirToDownload.listFiles();//get files from default Downloads directory before start export
//        assert listFiles != null;
//        for (int i = 0; i < waitTimeOut / 10; i++) {
//        }
//        try {
//            Process processSave = Runtime.getRuntime()
//                    .exec(new String[]{System.getProperty("user.dir") + "\\tools\\SaveIE.exe"
//                            , waitTimeOut.toString(), fileName, "param1"});//launch save script
//            processSave.waitFor();
//        } catch (Exception ex) {
//            throw new Exception("Can't save file!");
//        }
//        File myFile;
//        File[] listFilesAfterExport;
//        int count = 0;
//        do {
//            if (count == waitTimeOut) {
//                throw new Exception("File was not downloaded after timeout");
//            }
//            Thread.sleep(1000);
//            count++;
//            listFilesAfterExport = dirToDownload.listFiles();//get files from Downloads directory after start export
//            assert listFilesAfterExport != null;
//            myFile = Arrays.asList(listFilesAfterExport).stream() //get our file from list by last modified date
//                    .reduce((p1, p2) -> p1.lastModified() > p2.lastModified() ? p1 : p2).get();
//        } while (myFile.getName().contains(".partial"));//wait until file downloaded
//        if (listFiles.length >= listFilesAfterExport.length) {//check if count files is changed
//            throw new Exception("File not exist in folder 'Downloads'");
//        }
//        try {
//            Path path = Paths.get(fileName);//path to our download directory
//            Files.move(myFile.toPath(), path);//move and rename file to our download directory
//        } catch (Exception e) {
//            throw new Exception("Can't move file " + fileName + " !!! Cused by: <b>" + e.getMessage() + "<//b>");
//        }
//        new File(fileName);
//        for (int i = 0; i < waitTimeOut; i++) {
//            Thread.sleep(1000);
//            if (new File(fileName).isFile())//wait file
//                break;
//        }
//        Process processClose = Runtime.getRuntime()
//                .exec(new String[]{System.getProperty("user.dir") + "\\tools\\CloseSaveDialogIE9.exe"});//close dialog
//        processClose.waitFor();
//    }
//
//    /**
//     * Save file in Chrome browser
//     * run AutoIt script (individual for each project)
//     *
//     * @param fileName
//     * @param waitTimeOut
//     * @throws Exception
//     */
//    private static void SaveCH(String fileName, Integer waitTimeOut) throws Exception {
//        Process processSave = Runtime.getRuntime()
//                .exec(new String[]{System.getProperty("user.dir") + "\\tools\\SaveCH.exe", waitTimeOut.toString(), fileName});
//        processSave.waitFor();
//        Path path = Paths.get(fileName);//
//        for (int i = 0; i < waitTimeOut; i++) {
//            Thread.sleep(1000);
//            if (new File(fileName).isFile())
//                if (Files.size(path) != 0)//wait until file downloaded
//                    break;
//        }
//    }
//
//    /**
//     * Save file in Mozilla Firefox browser
//     * run AutoIt script (individual for each project)
//     *
//     * @param fileName
//     * @param waitTimeOut
//     * @throws Exception
//     */
//    private static void SaveFF(String fileName, Integer waitTimeOut) throws Exception {
//        Process processSave = Runtime.getRuntime()
//                .exec(new String[]{System.getProperty("user.dir") + "\\tools\\SaveFF.exe", waitTimeOut.toString(), fileName});
//        processSave.waitFor();
//        new File(fileName);
//        Path path = Paths.get(fileName);
//        for (int i = 0; i < waitTimeOut; i++) {
//            Thread.sleep(1000);
//            if (Files.size(path) != 0)//wait until file downloaded
//                break;
//        }
//    }
//
//    /**
//     * Check alert existence
//     *
//     * @return Boolean
//     */
//    private static Boolean IsAlertPresent() {
//        try {
//            getDriver().switchTo().alert();
//            return true;
//        } catch (Exception ex) {
//            return false;
//        }
//    }
//}
