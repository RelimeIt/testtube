package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class CheckBox extends CheckBoxesMethods {
    /**
     * Constructor
     * @param element web element
     */
    public CheckBox(WebElement element) {
        super(element);
    }
    /**
     * Constructor
     * @param element web element
     * @param name name of element
     */
    public CheckBox(WebElement element, String name) {
        super(element, name);
    }
    /**
     * Constructor
     * @param by class object
     */
    public CheckBox(By by) {
        super(by);
    }
}
