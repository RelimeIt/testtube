package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import com.google.common.collect.Collections2;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class DropDown extends Element {
    private org.openqa.selenium.support.ui.Select innerSelect;
    private WebElement element;

    /**
     * Constructor
     * @param element
     */
    public DropDown(WebElement element) {
        super(element);
        this.element = element;
    }

    /**
     * Constructor
     * @param element
     * @param name
     */
    public DropDown(WebElement element, String name) {
        super(element, name);
        this.element = element;
    }

    /**
     * Constructor
     * @param by
     */
    public DropDown(By by) {
        super(by);
        this.element = element;
    }

    /**
     * Method is used to perform proxy initialize, providing helper methods to select and deselect options.
     */
    private void proxyInitializer() {
        if (innerSelect == null)
            innerSelect = new org.openqa.selenium.support.ui.Select(element);
    }

    /**
     * Method is used to get list of options.
     * @return List<String> This returns list of options.
     */
    public List<String> options() {
        proxyInitializer();
        return innerSelect.getOptions().stream().map(p -> p.getText().trim()).collect(Collectors.toList());
    }

    /**
     * Method is used to get name of selected option
     * @return String This returns name of selected option
     * @throws Exception
     */
    public String selectedOption() throws Exception {
        return getFirstSelectedOption().getText().trim();
    }

    /**
     * Method is used to verify if specific option is present within list of options.
     * @param optionName
     * @return boolean
     */
    public boolean isOptionPresent(String optionName) {
        proxyInitializer();
        return options().contains(optionName.trim());
    }

    /**
     * Method is used to verify if dropdown is multiple
     * @return boolean
     */
    public boolean isMultiple() {
        proxyInitializer();
        return innerSelect.isMultiple();
    }

    /**
     * Method is used to deselect option by index
     * @param index Index of option
     * @throws Exception
     */
    public void deselectByIndex(int index) throws Exception {
        proxyInitializer();
        innerSelect.deselectByIndex(index);
        TypeFactory.getArpReportClient().ReportAction(String.format("Option of %s is deselected by index %s.", getName(), index), true);
    }

    /**
     * Method is used to select option by value from the dropdown
     * @param value Value of option
     * @throws Exception
     */
    public void selectByValue(String value) throws Exception {
        proxyInitializer();
        innerSelect.selectByValue(value);
        TypeFactory.getArpReportClient().ReportAction(String.format("Option of %s is selected by value %s.", getName(), value), true);
    }

    /**
     * Method is used to receive first selected option from the dropdown
     * @return WebElement
     * @throws Exception
     */
    public WebElement getFirstSelectedOption() throws Exception {
        proxyInitializer();
        WebElement el = innerSelect.getFirstSelectedOption();
        TypeFactory.getArpReportClient().ReportAction(String.format("First selected option for %s is defined.", getName()), true);
        return el;
    }

    /**
     * Method is used to select option by visible text from the dropdown.
     * If click invokers are set to the element, then invokers will be applied.
     * @param text Value of option
     * @throws Exception
     */
    public void selectByVisibleText(String text) throws Exception {
        proxyInitializer();

        if (invokers != null) {
            invokers.beforeClick(element);
            innerSelect.selectByVisibleText(text);
            invokers.afterClick(element);
        } else {
            innerSelect.selectByVisibleText(text);
        }
        validate(selectedOption().equals(text.trim()), text + " option is selected.");
    }

    /**
     * Method is used to select option by visible text from the dropdown.
     * If click invokers are set to the element, then invokers will be applied.
     * @param text Value of option
     * @param ignoreCase
     * @param ignoreValidation
     * @throws Exception
     */
    public void selectByVisibleText(String text, boolean ignoreCase, boolean ignoreValidation) throws Exception {
        proxyInitializer();
        if (ignoreCase) {
            int index = new ArrayList<String>(Collections2.transform(options(), p -> p.toLowerCase().trim())).indexOf(text.trim().toLowerCase());

            if (invokers != null) {
                invokers.beforeClick(element);
                innerSelect.selectByIndex(index);
                invokers.afterClick(element);
            } else {
                innerSelect.selectByIndex(index);
            }

        } else {
            if (invokers != null) {
                invokers.beforeClick(element);
                innerSelect.selectByVisibleText(text);
                invokers.afterClick(element);
            } else {
                innerSelect.selectByVisibleText(text);
            }
        }

        if (!ignoreValidation) {
            validate(selectedOption().toLowerCase().equals(text.toLowerCase().trim()), text + " option is selected.");
        }
    }

    /**
     * Method is used to deselect option by value.
     * @param value Value of option
     * @throws Exception
     */
    public void deselectByValue(String value) throws Exception {
        proxyInitializer();
        innerSelect.deselectByValue(value);
        TypeFactory.getArpReportClient().ReportAction(String.format("Option of %s is deselected by value %s.", getName(), value), true);
    }

    /**
     * Method is used to deselect all option from dropdown.
     * @throws Exception
     */
    public void deselectAll() throws Exception {
        proxyInitializer();
        innerSelect.deselectAll();
        TypeFactory.getArpReportClient().ReportAction(String.format("All options of %s were deselected.", getName()), true);
    }

    /**
     * Method is used to receive all selected options in dropdown.
     * @return List<WebElement> This returns list of all selected options in dropdown
     */
    public List<WebElement> getAllSelectedOptions() {
        proxyInitializer();
        return innerSelect.getAllSelectedOptions();
    }

    /**
     * Method is used to receive all options in dropdown.
     * @return List<WebElement> This returns list of all options in dropdown.
     */
    public List<WebElement> getOptions() {
        proxyInitializer();
        return innerSelect.getOptions();
    }

    /**
     * Method is used to deselect option by visible text in dropdown.
     * @param text Value of option
     * @throws Exception
     */
    public void deselectByVisibleText(String text) throws Exception {
        proxyInitializer();
        innerSelect.deselectByVisibleText(text);
        TypeFactory.getArpReportClient().ReportAction(String.format("Option of %s is deselected by text %s.", getName(), text), true);
    }

    /**
     * Method is used to select option by index.
     * @param index Index of option
     * @throws Exception
     */
    public void selectByIndex(int index) throws Exception {
        proxyInitializer();
        innerSelect.selectByIndex(index);
        TypeFactory.getArpReportClient().ReportAction(String.format("Option of %s is selected by index %s.", getName(), index), true);
    }

    /**
     * Method is used to select option by partial text.
     * @param text Partial text of option
     * @throws Exception
     */
    public void selectByPartialText(String text) throws Exception {
        proxyInitializer();
        for (WebElement option : innerSelect.getOptions()) {
            if (option.getText().contains(text)) {
                innerSelect.selectByIndex(innerSelect.getOptions().indexOf(option));
                break;
            }
        }
        TypeFactory.getArpReportClient().ReportAction(String.format("Option of %s is selected by partial text %s.", getName(), text), true);
    }
}
