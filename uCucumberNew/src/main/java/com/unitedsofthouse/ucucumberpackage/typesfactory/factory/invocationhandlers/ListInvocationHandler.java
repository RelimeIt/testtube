package com.unitedsofthouse.ucucumberpackage.typesfactory.factory.invocationhandlers;

import com.google.common.collect.Collections2;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.FactoryUtils.createNewInstance;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class ListInvocationHandler<T extends Element> implements InvocationHandler {
    private final ElementLocator locator;
    private final Class<T> clazz;
    private final String name;

    public ListInvocationHandler
            (ElementLocator elementLocator, Class<T> htmlType, String name) {
        locator = elementLocator;
        this.clazz = htmlType;
        this.name = name;
    }


    /**
     * Looking list of web Elements, process it and returns
     * new list with Custom type elements
     *
     * @param object
     * @param method
     * @param objects
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object object, Method method, Object[] objects) throws Throwable {

        List<WebElement> list = locator.findElements();

        List<T> toReturn = new ArrayList<>(Collections2.transform(list,
                webElement -> createNewInstance(clazz, webElement, name)));
        try {
            return method.invoke(toReturn, objects);
        } catch (InvocationTargetException e) {
            TypeFactory.getArpReportClient().ReportAction(name + " can't be created.", false);
            throw e.getCause();
        }
    }
}
