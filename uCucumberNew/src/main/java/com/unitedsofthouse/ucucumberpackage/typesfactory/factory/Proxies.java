package com.unitedsofthouse.ucucumberpackage.typesfactory.factory;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.invocationhandlers.HTMLInvocationHandler;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.invocationhandlers.ListInvocationHandler;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class Proxies {


    /**
     * Returns WebElement, an instance of a proxy class for the specified interfaces
     * that dispatches method invocations to the specified invocation handler
     * @param loader
     * @param locator
     * @return WebElement
     */
    public static WebElement proxyForLocator(ClassLoader loader, ElementLocator locator) {
        InvocationHandler handler = new LocatingElementHandler(locator);

        WebElement proxy;
        proxy = (WebElement) Proxy.newProxyInstance(
                loader, new Class[]{WebElement.class, WrapsElement.class, Locatable.class}, handler);
        return proxy;
    }


    /**
     * Create new proxy instance for custom HTML element
     * @param loader
     * @param locator
     * @param HTMLType
     * @param name
     * @param <T>
     * @return <T extends Element> Element
     */
    @SuppressWarnings("unchecked")
    public static <T extends Element> Element HTMLProxyForLocator(ClassLoader loader, ElementLocator locator, Class<T> HTMLType, String name) {

        InvocationHandler handler = new HTMLInvocationHandler<>(locator,name);

        WebElement proxy = (WebElement) Proxy.newProxyInstance(
                loader, new Class[]{WebElement.class, WrapsElement.class, Locatable.class}, handler);

        return FactoryUtils.createNewInstance(HTMLType, proxy, name);
    }


    /**
     * Create new proxy instance for list of custom HTML elements
     * @param loader ClassLoader loader
     * @param locator ElementLocator locator
     * @param HTMLType final Class<T> HTMLType
     * @param name String name
     * @param <T> Class type of element
     * @return <T extends Element> List<T>
     */
    @SuppressWarnings("unchecked")
    public static <T extends Element> List<T> HTMLProxyForListLocator(ClassLoader loader, ElementLocator locator, final Class<T> HTMLType, String name) {
        InvocationHandler handler = new ListInvocationHandler(locator, HTMLType, name);

        List<T> proxy = (List<T>) Proxy.newProxyInstance(loader, new Class[]{List.class}, handler);
        return proxy;
    }
}
