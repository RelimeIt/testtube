package com.unitedsofthouse.ucucumberpackage.typesfactory.types.tables;

import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.Container;
import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.FindIn;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.FactoryUtils.createNewInstance;
import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.getArpReportClient;

public class Cell {

    @Container(name = "Cell")
    public HTMLContainer containerMain;

    /**
     * Constructor
     * @param _container
     */
    public Cell(WebElement _container) {
        this.containerMain = new HTMLContainer(_container);
        TypeFactory.containerInitHTMLElements(this);
    }

    @FindIn(value = ".//a", containerName = "Cell")
    public List<Link> linksLinks;

    @FindIn(value = ".//input[@type='BUTTON' or @type='button']", containerName = "Cell")
    public List<Button> listButtons;

    @FindIn(value = ".//input[@type='TEXT' or @type='text']", containerName = "Cell")
    public List<TextInput> listTextInputs;

    @FindIn(value = ".//textarea", containerName = "Cell")
    public List<TextArea> listTextAreas;

    @FindIn(value = ".//input[@type='CHECKBOX' or @type='checkbox']", containerName = "Cell")
    public List<CheckBox> listCheckBoxes;

    @FindIn(value = ".//input[@type='file']", containerName = "Cell")
    public List<PlaceHolder> UploadFileElements;

    @FindIn(value = ".//img", containerName = "Cell")
    public List<Image> listImages;

    @FindIn(value = ".//input[@type='RADIO' or @type='radio']", containerName = "Cell")
    public List<RadioButton> listRadioButtons;

    /**
     * Get list of links, which contains text (linkName)
     *
     * @param linkName
     * @return List<Link> List of links, which contains text
     * @throws Exception
     */
    public List<Link> takeLinks(String linkName) throws Exception {
        getArpReportClient().ReportAction(String.format("Get links %1$s from current cell.", linkName), true);
        List<Link> links = containerMain.findElements(By.xpath(String.format(".//a[contains(text(), '%1$s')]", linkName)))
                .stream().map(e -> createNewInstance(Link.class, e, linkName + " link.")).collect(Collectors.toList());
        return links;
    }

    /**
     * Get list of dropDowns
     *
     * @return list of DropDowns
     * @throws Exception
     */
    public ArrayList<DropDown> takeDropDowns() throws Exception {
        getArpReportClient().ReportAction("Get dropdowns from current cell.", true);
        List<WebElement> selects = containerMain.findElements(By.xpath(".//select"));
        return selects.stream().map(DropDown::new).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * This method is used to get text from cell
     *
     * @return String Text of element
     * @throws Exception
     */
    public String text() throws Exception {
        return containerMain.getText();
    }

    /**
     * This method is used to get value from 'value' attribute
     *
     * @return value of @value attribute
     * @throws Exception
     */
    public String value() throws Exception {
        return containerMain.getAttribute("value");
    }

    /**
     * Return list of Web elements by xpath locator
     *
     * @param xpathLocator (String)
     * @return List<WebElement>
     * @throws Exception
     */
    public List<WebElement> takeCustomElements(String xpathLocator) throws Exception {
        return containerMain.findElements(By.xpath(xpathLocator));
    }
}