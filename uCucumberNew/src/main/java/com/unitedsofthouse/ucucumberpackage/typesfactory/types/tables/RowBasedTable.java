package com.unitedsofthouse.ucucumberpackage.typesfactory.types.tables;

import com.google.common.collect.Collections2;
import com.unitedsofthouse.ucucumberpackage.typesfactory.invokeclasses.Invoke;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.PlaceHolder;
import org.apache.commons.collections4.ListUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.getArpReportClient;

public class RowBasedTable<T extends Cell> extends Invoke {

    private WebElement containerMain;
    private By columnsName = By.xpath(".//td[1]");
    private By rowsLocator = By.xpath(".//tbody//tr");
    private By cellLocator = By.xpath(".//td");
    private Class<T> cellType;

    /**
     * Constructor
     * @param containerMain
     * @throws Exception
     */
    public RowBasedTable(Element containerMain) throws Exception {
        this.containerMain = containerMain.getWrappedElement();
        getArpReportClient().ReportAction(containerMain.getName() + " row based table initialization is complete.", true);
    }

    /**
     * Method is used to set rows locator
     * @param rows
     */
    public void setRowsLocator(By rows) {
        this.rowsLocator = rows;
    }

    /**
     * Method is used to set column names locator
     * @param columnsName
     */
    public void setColumnsNameLocator(By columnsName) {
        this.columnsName = columnsName;
    }

    /**
     * Method is used to set cells locator
     * @param cell
     */
    public void setCellLocator(By cell) {
        this.cellLocator = cell;
    }

    /**
     * Constructor
     * @param clazz
     * @param containerMain
     * @throws Exception
     */
    public RowBasedTable(Class<T> clazz, Element containerMain) throws Exception {

        cellType = clazz;
        this.containerMain = containerMain.getWrappedElement();
        getArpReportClient().ReportAction(containerMain.getName() + " row based table initialization is complete.", true);
    }

    /**
     * Method is used to get list of rows
     * @return List<WebElement>
     */
    public List<WebElement> rows() {
        return containerMain.findElements(rowsLocator);
    }

    /**
     * Method is used to get list of rows with cells
     * @return List<List<Cell>>
     */
    public List<List<Cell>> listRowsWithCells() {
        List<List<Cell>> lists = new ArrayList<>();
        for (WebElement row : rows()) {
            List<WebElement> elements = row.findElements(cellLocator);
            Collection<Cell> cell = Collections2.transform(elements, Cell::new);
            lists.add(new ArrayList<>(cell));
        }
        return lists;
    }

    /**
     * Method is used to get list of column's names
     * @return List<String>
     */
    public List<String> listColumnName() {
        Collection<String> toReturn = Collections2.transform(rows(),
                p -> p.findElement(columnsName).getText().trim());
        return new ArrayList<>(toReturn);
    }

    /**
     * Method is used to get specific cell
     * @param columnName
     * @param index
     * @return Cell
     * @throws Exception
     */
    public Cell takeCell(String columnName, int index) throws Exception {
        Cell cell = null;
        try {
            int ind = ListUtils.indexOf(listColumnName(), p -> p.contains(columnName));
            Collection<Cell> cells = Collections2.transform(rows().get(ind).findElements(cellLocator), Cell::new);
            cell = new ArrayList<>(cells).get(index);
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to get a specific cell
     * @param columnName
     * @param rowIndex
     * @return T
     * @throws Exception
     */
    public T takeCellSpecific(String columnName, int rowIndex) throws Exception {
        T cell = null;
        try {
            int ind = ListUtils.indexOf(listColumnName(), p -> p.contains(columnName));
            List<WebElement> cells = rows().get(ind).findElements(cellLocator);
            cell = cellType.getConstructor(WebElement.class).newInstance(cells.get(rowIndex));
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to create a specific element
     * @param element
     * @return T
     * @throws Exception
     */
    private T createCellSpecific(WebElement element) throws Exception {
        T cell = null;
        try {
            cell = cellType.getConstructor(WebElement.class).newInstance(element);
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to verify if table is enabled
     * @return boolean
     */
    public boolean isEnabled() {
        if (invokers != null) {
            return invokers.isElementEnabled(containerMain);
        }
        return containerMain.isEnabled();
    }

    /**
     * Method is used to verify if table is disabled
     * @return boolean
     */
    public boolean isDisabled() {
        if (invokers != null) {
            return invokers.isElementDisplayed(containerMain);
        }
        return containerMain.isDisplayed();
    }

    /**
     * Method is used to get specific element
     * @param element
     * @param columnName
     * @param rowIndex
     * @param position
     * @param <E>
     * @return <E extends Element> E
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public <E extends Element> E getElementSpecific(Class<E> element, String columnName, int rowIndex, int position) throws Exception {
        T cell = takeCellSpecific(columnName, rowIndex);
        switch (element.getSimpleName()) {
            case "DropDown":
                return element.cast(cell.takeDropDowns().get(position));
            case "Link":
                return element.cast(cell.linksLinks.get(position));
            case "TextInput":
                return element.cast(cell.listTextInputs.get(position));
            case "TextArea":
                return element.cast(cell.listTextAreas.get(position));
            case "CheckBox":
                return element.cast(cell.listCheckBoxes.get(position));
            case "Image":
                return element.cast(cell.listImages.get(position));
            case "Radio":
                return element.cast(cell.listRadioButtons.get(position));
        }
        return (E) new PlaceHolder(cell.containerMain.getWrappedElement());
    }
}