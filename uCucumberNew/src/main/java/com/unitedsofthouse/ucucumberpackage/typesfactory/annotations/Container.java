package com.unitedsofthouse.ucucumberpackage.typesfactory.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * interface container - describe container which contains another web elements
 * used to find web elements in difficult of approach places
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Container {
    /**
     * container name, all container names must be unique
     * because we call container by its name
    */
    String name();
}