package com.unitedsofthouse.ucucumberpackage.typesfactory.types.tables;

import com.google.common.collect.Collections2;
import com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.invokeclasses.Invoke;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.PlaceHolder;
import arp.CucumberArpReport;
import org.apache.commons.collections4.ListUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.getArpReportClient;

public class HeaderBasedTable<T extends Cell> extends Invoke {

    public WebElement container;
    protected Class<T> cellType;

    //Default values for table
    protected By headersNameLocator = By.xpath(".//thead//th");
    protected By cellLocator = By.xpath(".//td");
    protected By rowLocator = By.xpath(".//tbody//tr");
    protected List<String> HeadersList = null;

    /**
     * Constructor
     *
     * @param clazz
     * @param table
     * @throws Exception
     */
    public HeaderBasedTable(Class<T> clazz, Element table) throws Exception {
        cellType = clazz;
        container = table.getWrappedElement();
        getArpReportClient().ReportAction(table.getName() + " header based table initialization complete.", true);
    }

    /**
     * Constructor
     *
     * @param clazz
     * @param table
     * @param headersNames
     * @throws Exception
     */
    public HeaderBasedTable(Class<T> clazz, Element table, List<String> headersNames) throws Exception {
        cellType = clazz;
        container = table.getWrappedElement();
        HeadersList = headersNames;
        getArpReportClient().ReportAction(table.getName() + " header based table initialization complete.", true);
    }

    /**
     * Method is used to set locator for headers name
     *
     * @param headersNameLocator
     */
    public void setHeadersNameLocator(By headersNameLocator) {
        this.headersNameLocator = headersNameLocator;
    }

    /**
     * Method is used to set locator for cell
     *
     * @param cellLocator
     */
    public void setCellLocator(By cellLocator) {
        this.cellLocator = cellLocator;
    }

    /**
     * Method is used to set locator for row
     *
     * @param rowLocator
     */
    public void setRowLocator(By rowLocator) {
        this.rowLocator = rowLocator;
    }

    /**
     * Method is used to get list of names of header
     *
     * @return List<String> List of names of header
     */
    public List<String> List_Headers() {
        if (HeadersList == null) {
            List<WebElement> elements = container.findElements(headersNameLocator);
            return elements.stream()
                    .map(p -> p.getText().replace("\n", " ").trim()).collect(Collectors.toCollection(ArrayList::new));
        }
        return HeadersList;
    }

    /**
     * Method is used to get list of cells of header
     *
     * @return List<Cell> List of cells of header
     */
    public List<Cell> listHeadersCells() {
        List<WebElement> elements = container.findElements(headersNameLocator);
        return elements.stream().map(Cell::new).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method is used to get list of strings of column
     *
     * @return ArrayList<String>
     */
    public ArrayList<String> listColumn() {
        ArrayList<String> list = new ArrayList<>();
        List<WebElement> elements;
        if (!cellLocator.equals(By.xpath(".//td"))) {
            elements = container.findElements(cellLocator);
        } else {
            elements = container.findElements(By.xpath(".//td[1]"));
        }
        list.addAll(elements.stream().map(p -> p.getText().trim()).collect(Collectors.toList()));
        return list;
    }

    /**
     * Method is used to get list of strings of specific column.
     *
     * @param columnName
     * @return ArrayList<String>
     */
    public ArrayList<String> listColumn(String columnName) {
        int index = List_Headers().indexOf(columnName);
        List<WebElement> elements = container.findElements(rowLocator).stream().map(p -> p.findElements(cellLocator).get(index)).collect(Collectors.toList());
        return elements.stream().map(p -> p.getText().trim()).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Returns column with removed delimiterGroups as digit separator (for example 123,456.00 converts to 123456.00)
     * String delimiterGroups - symbol which used to split groups of digits
     * String delimiterDecimal - symbol which used to split decimal part
     */
    public ArrayList<String> listColumn(String columnName, String delimiterGroups, String delimiterDecimal) {
        String pattern = ".*(\\d+\\" + delimiterGroups + "\\d*\\" + delimiterDecimal + "*\\d*).*";
        Pattern r = Pattern.compile(pattern);

        ArrayList<String> list = new ArrayList<>();
        int index = List_Headers().indexOf(columnName);
        List<WebElement> elements = container.findElements(rowLocator).stream().map(p -> p.findElements(cellLocator).get(index)).collect(Collectors.toList());
        for (WebElement element : elements) {
            Matcher m = r.matcher(element.getText().trim());
            if (m.find()) {
                list.add(element.getText().replace(delimiterGroups, "").trim());
            } else {
                list.add(element.getText().trim());
            }
        }
        return list;
    }


    /**
     * Method is used to get list of rows that presented as list of WebElements.
     *
     * @return List<WebElement>
     */
    public List<WebElement> listRows() {
        return container.findElements(rowLocator);
    }


    /**
     * Method is used to get list of column cells for specific column.
     *
     * @param columnName
     * @return ArrayList<Cell>
     */
    public ArrayList<Cell> listColumnCells(String columnName) {
        int index = List_Headers().indexOf(columnName);
        List<WebElement> elements = container.findElements(rowLocator).stream().map(p -> p.findElements(cellLocator).get(index)).collect(Collectors.toList());
        return elements.stream().map(Cell::new).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method is used to get list of column cells for specific column.
     *
     * @param columnName
     * @return ArrayList<T>
     */
    public ArrayList<T> listColumnCellsSpecific(String columnName) {
        int index = List_Headers().indexOf(columnName);
        List<WebElement> elements = container.findElements(rowLocator).stream().map(p -> p.findElements(cellLocator).get(index)).collect(Collectors.toList());
        return elements.stream().map(p -> {
            try {
                return cellType.getConstructor(WebElement.class).newInstance(p);
            } catch (Throwable e) {
                return null;
            }
        }).collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * Method is used to get cell for specific column (by column name) and row (by row index)
     *
     * @param columnName
     * @param rowIndex
     * @return Cell
     * @throws Exception
     */
    public Cell takeCell(String columnName, int rowIndex) throws Exception {
        Cell cell = null;
        try {
            int colindex = List_Headers().indexOf(columnName);
            WebElement container = this.container.findElements(rowLocator).get(rowIndex).findElements(cellLocator).get(colindex);
            cell = new Cell(container);
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to get cell for specific column (by column name) and row (by row name)
     *
     * @param columnName
     * @param rowName
     * @return Cell
     * @throws Exception
     */
    public Cell takeCell(String columnName, String rowName) throws Exception {
        Cell cell = null;
        try {
            int colindex = List_Headers().indexOf(columnName);
            int rowIndex = ListUtils.indexOf(listRows(), p -> p.getText().contains(rowName));
            WebElement container = this.container.findElements(rowLocator).get(rowIndex).findElements(cellLocator).get(colindex);
            cell = new Cell(container);
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to get cell for specific column (by column index) and row (by row index)
     *
     * @param columnIndex
     * @param rowIndex
     * @return
     * @throws Exception
     */
    public Cell takeCell(int columnIndex, int rowIndex) throws Exception {
        Cell cell = null;
        try {
            WebElement container = this.container.findElements(rowLocator).get(rowIndex).findElements(cellLocator).get(columnIndex);
            cell = new Cell(container);
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to verify if row in table is empty
     *
     * @return boolean
     */
    public boolean isRowsIsEmpty() {
        return Collections2.filter(listRows(),
                input -> !input.getAttribute("class").contains("hide")).size() == 0;
    }

    /**
     * Method is used to verify if row in table is empty by checking that specific attribute contains specific value
     *
     * @param atribute
     * @param value
     * @return boolean
     */
    public boolean isRowsIsEmpty(String atribute, String value) {
        return Collections2.filter(listRows(),
                p -> !p.getAttribute(atribute).contains(value)).size() == 0;
    }

    /**
     * Method is used to get specific cell
     *
     * @param columnName
     * @param rowIndex
     * @return T
     * @throws Exception
     */
    public T takeCellSpecific(String columnName, int rowIndex) throws Exception {
        T cell = null;
        try {
            int colindex = List_Headers().indexOf(columnName);
            WebElement container = this.container.findElements(rowLocator).get(rowIndex).findElements(cellLocator).get(colindex);
            cell = cellType.getConstructor(WebElement.class).newInstance(container);
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to get specific cell
     *
     * @param columnName
     * @param rowName
     * @return T
     * @throws Exception
     */
    public T takeCellSpecific(String columnName, String rowName) throws Exception {
        T cell = null;
        try {
            int colindex = List_Headers().indexOf(columnName);
            int rowIndex = listColumn().indexOf(rowName);
            WebElement container = this.container.findElements(rowLocator).get(rowIndex).findElements(cellLocator).get(colindex);
            cell = cellType.getConstructor(WebElement.class).newInstance(container);
        } catch (Exception e) {
            getArpReportClient().ReportAction("Cell was not created.", false);
        }
        return cell;
    }

    /**
     * Method is used to verify if table is enabled
     *
     * @return boolean
     */
    public boolean isEnabled() {
        if (invokers != null) {
            return invokers.isElementEnabled(container);
        }
        return container.isEnabled();
    }

    /**
     * Method is used to verify if table is disabled
     *
     * @return boolean
     */
    public boolean isDisabled() {
        if (invokers != null) {
            return invokers.isElementDisplayed(container);
        }
        return container.isDisplayed();
    }

    /**
     * Method is used to get specific element
     *
     * @param element
     * @param columnName
     * @param rowIndex
     * @param position
     * @param <E>
     * @return <E extends Element> E
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public <E extends Element> E getElementSpecific(Class<E> element, String columnName, int rowIndex, int position) throws Exception {

        T cell = takeCellSpecific(columnName, rowIndex);

        switch (element.getSimpleName()) {
            case "DropDown":
                return element.cast(cell.takeDropDowns().get(position));
            case "Link":
                return element.cast(cell.linksLinks.get(position));
            case "TextInput":
                return element.cast(cell.listTextInputs.get(position));
            case "TextArea":
                return element.cast(cell.listTextAreas.get(position));
            case "CheckBox":
                return element.cast(cell.listCheckBoxes.get(position));
            case "Image":
                return element.cast(cell.listImages.get(position));
            case "Radio":
                return element.cast(cell.listRadioButtons.get(position));
        }
        return (E) new PlaceHolder(cell.containerMain.getWrappedElement());
    }

    /**
     * Method is used to get specific element
     *
     * @param element
     * @param columnName
     * @param rowName
     * @param position
     * @param <E>
     * @return <E extends Element> E
     * @throws Throwable
     */
    public <E extends Element> E getElementSpecific(Class<E> element, String columnName, String rowName, int position) throws Throwable {
        int index = listColumn().indexOf(rowName);
        return getElementSpecific(element, columnName, index, position);
    }

    /**
     * Method is used to create cell
     *
     * @param elem WebElement
     * @return Cell
     * @throws Exception
     */
    private Cell createCell(WebElement elem) throws Exception {
        Cell cell = null;

        try {
            cell = (Cell) this.cellType.getConstructor(new Class[]{WebDriver.class, WebElement.class}).newInstance(new Object[]{WebCucDriver.getDriver(), elem});
        } catch (Exception var4) {
            TypeFactory.getArpReportClient();
            CucumberArpReport.ReportAction("Cell was not created.", false);
        }

        return cell;
    }
}
