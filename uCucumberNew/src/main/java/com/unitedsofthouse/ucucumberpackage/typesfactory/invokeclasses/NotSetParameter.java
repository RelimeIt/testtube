package com.unitedsofthouse.ucucumberpackage.typesfactory.invokeclasses;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * interface used to mark parameters (to use in refactoring)
 */
@Target(ElementType.PARAMETER)
public @interface NotSetParameter {
}
