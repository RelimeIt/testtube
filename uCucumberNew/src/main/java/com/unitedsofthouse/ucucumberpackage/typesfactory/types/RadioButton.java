package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class RadioButton extends Element {
    /**
     * Constructor
     * @param element web element
     */
    public RadioButton(WebElement element) {
        super(element);
    }
    /**
     * Constructor
     * @param element web element
     * @param name name of element
     */
    public RadioButton(WebElement element, String name) {
        super(element, name);
    }
    /**
     * Constructor
     * @param by class object
     */
    public RadioButton(By by) {
        super(by);
    }
}
