package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by a.danchenko on 12/2/2014.
 */
public class PlaceHolder extends Element {
    /**
     * Constructor
     * @param element web element
     */
    public PlaceHolder(WebElement element) {
        super(element);
    }
    /**
     * Constructor
     * @param element web element
     * @param name name of element
     */
    public PlaceHolder(WebElement element, String name) {
        super(element, name);
    }
    /**
     * Constructor
     * @param by class object
     */
    public PlaceHolder(By by) {
        super(by);
    }
}
