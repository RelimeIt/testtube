package com.unitedsofthouse.ucucumberpackage.typesfactory.invokeclasses;
/** abstract class to set invokers */
public abstract class Invoke {
    /** remembers added invokers */
    protected Invokers invokers;
    /**
     * setInvokers method adds described invokers
     *
     * @param invokers
     *            object with different kinds of invokers
     */
    public void setInvokers(Invokers invokers) {
        this.invokers = invokers;
    }
}
