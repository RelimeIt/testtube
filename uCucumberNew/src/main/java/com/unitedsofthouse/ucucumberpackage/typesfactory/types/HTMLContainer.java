package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class HTMLContainer extends Element {
    /**
     * Constructor
     * @param element web element
     */
    public HTMLContainer(WebElement element) {
        super(element);
    }
    /**
     * Constructor
     * @param element web element
     * @param name name of element
     */
    public HTMLContainer(WebElement element, String name) {
        super(element, name);
    }
    /**
     * Constructor
     * @param by class object
     */
    public HTMLContainer(By by) {
        super(by);
    }
}
