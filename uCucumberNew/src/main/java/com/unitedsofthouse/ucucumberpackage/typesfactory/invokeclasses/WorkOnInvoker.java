package com.unitedsofthouse.ucucumberpackage.typesfactory.invokeclasses;

import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.InvokeTo;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * class to process invokers
 */
public class WorkOnInvoker {
    /**
     * WorkOnInvokers method process invokers for all described elements on page
     *
     * @param page
     *          page with diferent elements
     */
    @SuppressWarnings("unchecked")
    public static void WorkOnInvokers(Object page) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        ArrayList<Field> fields = new ArrayList<>();
        List<Field> fieldsWithInvokers = new ArrayList<>();

        Class<?> currentPage = page.getClass();
        while (!currentPage.getSimpleName().equals("PagesInstances") && currentPage != Object.class) {
            fields.addAll(Arrays.asList(currentPage.getDeclaredFields()));
            currentPage = currentPage.getSuperclass();
        }
        if (fields.size() > 0) {
            fieldsWithInvokers.addAll(fields.stream().filter(invf -> invf.getDeclaredAnnotation(InvokeTo.class) != null).collect(Collectors.toList()));
        }

        if (fieldsWithInvokers.size() != 0) {
            for (Field field : fieldsWithInvokers) {
                String invokeToName = field.getDeclaredAnnotation(InvokeTo.class).elemet();
                if (fields.stream().anyMatch(p -> p.getName().equals(invokeToName))) {
                    field.setAccessible(true);
                    Field invokeToField = FieldUtils.getField(page.getClass(), invokeToName, true);
                    invokeToField.getType().getMethod("setInvokers", Invokers.class).invoke(invokeToField.get(page), field.get(page));
                }
            }
        }
    }
}
