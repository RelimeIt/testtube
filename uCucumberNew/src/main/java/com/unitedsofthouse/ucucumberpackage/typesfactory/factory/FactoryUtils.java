package com.unitedsofthouse.ucucumberpackage.typesfactory.factory;

import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.Container;
import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.FindIn;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.HTMLContainer;
import org.apache.commons.lang3.reflect.ConstructorUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class FactoryUtils {

    /**
     * Check that field is Container
     *
     * @param field          field to check
     * @param isFindByMustBe
     * @return boolean
     */
    public static boolean isHTMLContainer(Field field, boolean isFindByMustBe) {
        return field.isAnnotationPresent(Container.class) && field.isAnnotationPresent(FindBy.class) == isFindByMustBe && field.getType().equals(HTMLContainer.class);
    }

    public static List<Field> collectFields(Object page) {
        final List<Field> fields = new ArrayList<>();
        Class currentPageObject = page.getClass();
        WebElement element = null;

        while (currentPageObject != Object.class) {
            fields.addAll(new ArrayList<>(Arrays.asList(currentPageObject.getDeclaredFields())));
            currentPageObject = currentPageObject.getSuperclass();
        }

        return fields;
    }

    /**
     * Type erasure in Java isn't complete. Attempt to discover the generic
     * interfaceType of the list.
     *
     * @param field
     * @return Class<?>
     */
    public static Class<?> getErasureClass(Field field) {

        Type genericType = field.getGenericType();
        if (!(genericType instanceof ParameterizedType)) {
            return null;
        }
        return (Class<?>) ((ParameterizedType) genericType).getActualTypeArguments()[0];
    }

    /**
     * Check if list can be decorated
     *
     * @param type  type
     * @param field field to check
     * @return boolean
     */
    @SuppressWarnings("unchecked")
    public static boolean isDecoratableList(Class type, Field field) {

        if (!List.class.isAssignableFrom(field.getType())) {
            return false;
        }
        Class<?> listType = getErasureClass(field);
        if (listType != null && !type.isAssignableFrom(listType)) {
            return false;
        }
        Type genericType = field.getGenericType();
        if (!(genericType instanceof ParameterizedType)) {
            return false;
        }
        return (field.getAnnotation(FindIn.class) != null) || (field.getAnnotation(FindBy.class) != null);
    }

    /**
     * Creates class instance, that implement Class<T> clazz
     * and invoke constructor with WebElement argument
     *
     * @param clazz   class
     * @param element WebElement element
     * @param <T>     custom web Element
     * @return <T extends Element> T
     */
    @SuppressWarnings("unchecked")
    public static <T extends Element> T createNewInstance(Class<T> clazz, WebElement element) {
        T instance = null;

        Constructor<?> cons = ConstructorUtils.getAccessibleConstructor(clazz, WebElement.class);

        try {
            instance = (T) cons.newInstance(element);
        } catch (Exception e) {
            try {
                TypeFactory.getArpReportClient().ReportAction(String.format("WebElement can't be represented as ", clazz.getSimpleName()), false);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        return instance;
    }

    /**
     * Creates class instance, that implement Class<T> clazz
     * and invoke constructor with WebElement argument
     *
     * @param clazz   class
     * @param element WebElement element
     * @param <T>     custom web Element
     * @param name    element Name
     * @return <T extends Element> T
     */
    @SuppressWarnings("unchecked")
    public static <T extends Element> T createNewInstance(Class<T> clazz, WebElement element, String name) {
        T instance = null;

        Constructor<?> cons = ConstructorUtils.getAccessibleConstructor(clazz, WebElement.class, String.class);

        if (cons == null) {
            return createNewInstance(clazz, element);
        } else {
            try {
                instance = (T) cons.newInstance(element, name);
            } catch (Exception e) {
                try {
                    TypeFactory.getArpReportClient().ReportAction(String.format("WebElement can't be represented as", clazz.getSimpleName()), false);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return instance;
    }
}
