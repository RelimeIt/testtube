package com.unitedsofthouse.ucucumberpackage.typesfactory.invokeclasses;

import org.openqa.selenium.WebElement;

/**
 * interface with different kinds of invokers
 */
public interface Invokers {
    /**
     * @param arg text of web element
     * @return String with text of web element
     */
    default String readText(@NotSetParameter String arg) {
        return arg;
    }
    /**
     * @param element web element to clickOnElement on him
     */
    default void clickOnElement(@NotSetParameter WebElement element) {
        element.click();
    }
    /**
     * @param element web element to make some verification before clickOnElement
     */
    default void beforeClick(@NotSetParameter WebElement element) {
    }
    /**
     * @param element web element to make some verification after clickOnElement
     */
    default void afterClick(@NotSetParameter WebElement element) {
    }
    /**
     * @param element web element into which enter text
     * @param arg text to set into web element
     */
    default void enterText(@NotSetParameter WebElement element, @NotSetParameter String arg) {
    }
    /**
     * @param element web element (label)
     * @return String with text of label
     */
    default String getLabelText(@NotSetParameter WebElement element) {
        return "";
    }
    /**
     * @param element web element to chek
     * @return true - if element enable, false - if disabled
     */
    default boolean isElementEnabled(@NotSetParameter WebElement element) {
        return element.isEnabled();
    }
    /**
     * @param element web element to chek
     * @return true - if element displayed, false - if not displayed
     */
    default boolean isElementDisplayed(WebElement element) {
        return element.isDisplayed();
    }
    /**
     * @param element web element to chek
     * @return true - if element selected, false - if not selected
     */
    default boolean isElementSelected(@NotSetParameter WebElement element) {
        return element.isSelected();
    }
}
