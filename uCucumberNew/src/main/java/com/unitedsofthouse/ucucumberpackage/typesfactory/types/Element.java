package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.invokeclasses.Invoke;
import org.openqa.selenium.*;
import org.openqa.selenium.internal.WrapsElement;

import java.util.List;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;
import static org.stringtemplate.v4.StringRenderer.escapeHTML;
import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.getArpReportClient;

/**
 * <h1>Element class</h1>
 * Element class is used as skeleton class for all types of WebElements.
 * Created by pchelintsev on 12/7/2015.
 */
public abstract class Element extends Invoke implements WrapsElement {

    private final WebElement element;
    protected String name = "SET ELEMENT NAME";

    /**
     * This method is used to receive name of current Element
     * @return String This returns name of current element
     */
    public String getName() {
        return name;
    }

    /**
     * This method is used to set name of current Element
     */
    private void setName() {
        final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
        this.name = ste[4].getMethodName();
    }

    /**
     * Constructor
     * @param element
     */
    public Element(WebElement element) {
        this.element = element;
        setName();
    }

    /**
     * Constructor
     * @param element
     * @param name
     */
    public Element(WebElement element, String name) {
        this.element = element;
        this.name = name;
    }

    /**
     * Constructor
     * @param by
     */
    public Element(By by) {
        this.element = getDriver().findElement(by);
        setName();
    }

    /**
     * Constructor
     * @param by
     * @param name
     */
    public Element(By by, String name) {
        this.element = getDriver().findElement(by);
        this.name = name;
    }

    /**
     * This method is used to click on element.
     * If click invokers are set to the element, then invokers will be applied.
     * @throws Exception
     */
    public void click() throws Exception {
        if (invokers != null) {
            WebElement element = getWrappedElement();
            invokers.beforeClick(element);
            getArpReportClient().ReportAction("'Before clickOnElement' invoker was processed.", true);
            invokers.clickOnElement(element);
            getArpReportClient().ReportAction(getName() + " was clicked.", true);
            invokers.afterClick(element);
            getArpReportClient().ReportAction("'After clickOnElement' invoker was processed.", true);
        } else getWrappedElement().click();
        getArpReportClient().ReportAction(getName() + " was clicked.", true);
    }

    /**
     * This method is used to click on element and validate the action.
     * @param expected
     * @throws Exception
     */
    public void click(boolean expected) throws Exception {
        click();
        validate(expected, "clicked.");
    }

    /**
     * This method is used to verify if element is selected.
     * @param expected
     * @throws Exception
     */
    public void isSelected(boolean expected) throws Exception {
        validate(isSelected() == expected, "selected.");
    }

    /**
     * This method is used to send keys to the element.
     * @param keysToSend
     * @throws Exception
     */
    public void sendKeys(CharSequence... keysToSend) throws Exception {
        element.sendKeys(keysToSend);
        getArpReportClient().ReportAction("Key '" + keysToSend.toString() + "' was sent to " + getName(), true);
    }

    /**
     * This method is used to click an element
     * @throws Exception
     */
    public void submit() throws Exception {
        element.submit();
        getArpReportClient().ReportAction(getName() + " was submitted.", true);
    }

    /**
     * This method is used to get attribute value of element.
     * @param name Name of attribute
     * @return String This returns value of attribute
     * @throws Exception If attribute with @param name is absent, then exception will be thrown.
     */
    public String getAttribute(String name) throws Exception {
        getArpReportClient().ReportAction(String.format("Get '%1$s' for '%2$s'.", name, getName()), true);
        String attr = element.getAttribute(name);
        getArpReportClient().ReportAction(String.format("Attribute '%1$s' for '%2$s' has retrieved.", name, getName()), true);
        return attr;
    }

    /**
     * This method is used to verify if attribute is present in WebElement.
     * If this attribute is absent, then exception will be smothered.
     * @param attribute
     * @return boolean
     */
    public boolean isAttributePresent(String attribute) {
        Boolean result = false;
        try {
            String value = element.getAttribute(attribute);
            if (value != null) {
                result = true;
            }
        } catch (Exception e) {
            //To put out an exception if attribute is absent in WebElement
        }

        return result;
    }

    /**
     * This method is used to get Css value of element.
     * @param propertyName
     * @return String This returns value of Css
     * @throws Exception
     */
    public String getCssValue(String propertyName) throws Exception {
        getArpReportClient().ReportAction(String.format("Get '%1$s' for '%2$s'.", propertyName, getName()), true);
        String css = element.getCssValue(propertyName);
        getArpReportClient().ReportAction(String.format("Property '%1$s' for '%2$s' has retrieved.", propertyName, getName()), true);
        return css;
    }

    /**
     * This method is used to get size of element.
     * @return Dimension
     * @throws Exception
     */
    public Dimension getSize() throws Exception {
        getArpReportClient().ReportAction(String.format("Get size for '%1$s'.", getName()), true);
        Dimension size = element.getSize();
        getArpReportClient().ReportAction(String.format("Size of '%1$s' : '%2$s'.", getName(), size), true);
        return size;
    }

    /**
     * This method is used to find list of elements by specific locator
     * @param by
     * @return List<WebElement> This returns list of elements by specific locator
     * @throws Exception
     */
    public List<WebElement> findElements(By by) throws Exception {
        getArpReportClient().ReportAction(String.format("Find elements by locator '%1$s' from '%2$s'.", by, getName()), true);
        List<WebElement> list = element.findElements(by);
        getArpReportClient().ReportAction(String.format("'%1$s' elements were found by locator '%2$s' from '%3$s'.", list.size(), by, getName()), true);
        return list;
    }

    /**
     * This method is used to get text of element.
     * If readText invoker is set to the element, then invoker will be applied.
     * @return String This returns text of element.
     * @throws Exception
     */
    public String getText() throws Exception {
        getArpReportClient().ReportAction("Get text for "+ getName(), true);
        if (invokers != null) {
            return invokers.readText(escapeHTML(getWrappedElement().getText()));
        }
        String text = escapeHTML(getWrappedElement().getText());
        getArpReportClient().ReportAction(String.format("text of '%1$s' is '%2$s'.", getName(), text), true);
        return text;
    }

    /**
     * This method is used to get tag name of element.
     * @return String This returns tag name of element.
     * @throws Exception
     */
    public String getTagName() throws Exception {
        getArpReportClient().ReportAction(String.format("Get '%1$s' for '%2$s'.", "tag", getName()), true);
        String str = element.getTagName();
        getArpReportClient().ReportAction(String.format("'%1$s' of '%2$s' : '%3$s'.", "Tag", getName(), str), true);
        return str;
    }

    /**
     * This method is used to verify if element is selected
     * If isElementSelected invoker is set to the element, then invoker will be applied.
     * @return boolean This returns true if element is selected.
     * @throws Exception
     */
    public boolean isSelected() throws Exception {
        boolean isSelected;
        getArpReportClient().ReportAction(String.format("Get '%1$s' for '%2$s'.", "isSelected", getName()), true);
        if (invokers != null) {
            isSelected = invokers.isElementSelected(element);
        }

        isSelected = element.isSelected();
        getArpReportClient().ReportAction(String.format("'%1$s' of '%2$s' : '%3$s'.", "isSelected", getName(), isSelected), true);
        return isSelected;
    }

    /**
     * This method is used to find element by specific locator
     * @param by
     * @return WebElement This returns element by specific locator
     * @throws Exception
     */
    public WebElement findElement(By by) throws Exception {
        getArpReportClient().ReportAction(String.format("Find element by  '%1$s' from '%2$s'.", by, getName()), true);
        WebElement webElement = null;

        try {
            webElement = element.findElement(by);
        } catch (NoSuchElementException ex) {
            validate(false, "Cannot find an element.");
            throw new NoSuchElementException(ex.getLocalizedMessage());
        }
        getArpReportClient().ReportAction(String.format("Element by locator '%1$s' from '%2$s' were found.", by, getName()), true);
        return webElement;
    }

    /**
     * This method is used to verify if element is enabled.
     * If isElementEnabled invoker is set to the element, then invoker will be applied.
     * @return boolean
     * @throws Exception
     */
    public boolean isEnabled() throws Exception {
        boolean isEnabled;
        TypeFactory.getArpReportClient().ReportAction("Check 'isEnabled' for " + getName(), true);
        if (invokers != null) {
            isEnabled = invokers.isElementEnabled(element);
        }
        isEnabled = element.isEnabled();
        getArpReportClient().ReportAction(getName() + " isEnabled : " + isEnabled, true);
        return isEnabled;
    }

    /**
     * This method is used to verify if element is displayed.
     * If isElementDisplayed invoker is set to the element, then invoker will be applied.
     * @return boolean
     * @throws Exception
     */
    public boolean isDisplayed() throws Exception {
        boolean isDisplayed;
        TypeFactory.getArpReportClient().ReportAction("Check 'isDisplayed' for " + getName(), true);
        if (invokers != null) {
            isDisplayed = invokers.isElementDisplayed(element);
        }

        try {
            isDisplayed = element.isDisplayed();
        } catch (Exception ex) {
            isDisplayed = false;
        }
        getArpReportClient().ReportAction(getName() + " isDisplayed : " + isDisplayed, true);
        return isDisplayed;
    }

    /**
     * This method is used to verify if element is displayed with additional validation.
     * @param expected
     * @throws Exception
     */
    public void isDisplayed(boolean expected) throws Exception {
        validate(isDisplayed() == expected, "displayed.");
    }

    /**
     * This method is used to clear an element.
     * @throws Exception
     */
    public void clear() throws Exception {
        clear(true);
    }

    /**
     * This method is used to clear an element with additional validation..
     * @param expected
     * @throws Exception
     */
    public void clear(boolean expected) throws Exception {
        getArpReportClient().ReportAction("Clear " + getName(), true);
        element.clear();
        validate(getText().isEmpty() == expected, "cleared.");
    }

    /**
     * This method is used to get wrapped element
     * @return WebElement This returns wrapped element
     */
    public WebElement getWrappedElement() {
        return element;
    }

    /**
     * This method is used to validate result
     * @param expected
     * @param message
     * @throws Exception
     */
    public void validate(boolean expected, String message) throws Exception {
        String reportMessage = String.format("%1$s %2$s %3$s", name, expected ? "" : "not", message);
        if (expected) {
            getArpReportClient().ReportAction(reportMessage, true);
        } else
            getArpReportClient().ReportAction(reportMessage, false);
    }
}
