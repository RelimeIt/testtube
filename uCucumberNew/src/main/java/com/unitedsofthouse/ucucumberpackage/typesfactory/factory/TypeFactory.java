package com.unitedsofthouse.ucucumberpackage.typesfactory.factory;

import com.google.common.collect.Collections2;
import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.Container;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.fielddecorator.CustomFieldDecorator;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.fielddecorator.CustomHTMLFieldDecorator;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.HTMLContainer;
import arp.CucumberArpReport;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;
import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.FactoryUtils.collectFields;
import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.FactoryUtils.isHTMLContainer;
import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.Proxies.proxyForLocator;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class TypeFactory extends PageFactory {
    private TypeFactory() {
    }

    private static Map<String, WebElement> containerMap = new HashMap<>();

    private static CucumberArpReport arpReportClient;

    /**
     * This method is used get set report client of reporting system (ARP)
     * @return
     */
    public static CucumberArpReport getArpReportClient() {
        return arpReportClient;
    }

    /**
     * This method is used to set report client of reporting system (ARP)
     * @param arpReportClient
     */
    public static void setArpReportClient(CucumberArpReport arpReportClient) {
        TypeFactory.arpReportClient = arpReportClient;
    }

    /**
     * Fill container Map<String, WebElement> with values from page marked @Container
     *
     * @param page
     * @param isRoot
     */
    private static void collectContainers(Object page, final boolean isRoot) {
        final List<Field> fields = collectFields(page);
        WebElement element = null;
        Collection<Field> fieldCollection;
        if (isRoot) {
            fieldCollection = Collections2.filter(fields,
                    p -> (p.isAnnotationPresent(Container.class) && p.getAnnotations().length == 1));
        } else
            fieldCollection = Collections2.filter(fields,
                    p -> (p.isAnnotationPresent(Container.class) && p.getAnnotations().length >= 2));

        for (Field field : fieldCollection) {
            final Container containerAnnotation = field.getAnnotation(Container.class);
            final boolean isAccessible = field.isAccessible();
            try {
                field.setAccessible(true);
                containerMap.put(containerAnnotation.name(), (WebElement) field.get(page));
                //returns 'accessible' to previous state:
                field.setAccessible(isAccessible);
            } catch (IllegalAccessException e) {
                System.err.println("IllegalAccessException related to 'HTMLContainer' annotation");
            }
        }
    }

    /**
     * initializes fields annotated @Container
     *
     * @param driver
     * @param page
     */
    private static void initHTMLContainers(WebDriver driver, Object page) {
        final List<Field> fields = collectFields(page);
        WebElement element;
        ElementLocator locator;

        fields.removeIf(p -> !isHTMLContainer(p, true));

        for (Field field : fields) {
            locator = new DefaultElementLocatorFactory(driver).createLocator(field);
            element = proxyForLocator(page.getClass().getClassLoader(), locator);
            field.setAccessible(true);
            try {
                field.set(page, new HTMLContainer(element));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Look threw the page and collect fields, marked @Container annotation or have Container type
     *
     * @param page
     */
    private static void collectHTMLContainers(Object page) {
        Collection<Field> fieldCollection;

        final List<Field> fields = collectFields(page);
        WebElement element = null;

        fieldCollection = fields.stream().filter(p -> p.getType() == HTMLContainer.class)
                .filter(p -> p.getDeclaredAnnotation(Container.class) != null).collect(Collectors.toList());

        for (Field field : fieldCollection) {
            try {
                final Container containerAnnotation = field.getAnnotation(Container.class);
                field.setAccessible(true);
                if (field.getDeclaredAnnotation(FindBy.class) != null) {
                    element = ((HTMLContainer) field.get(page)).getWrappedElement();
                    containerMap.put(containerAnnotation.name(), element);
                } else if (field.get(page) != null) {
                    containerMap.put(containerAnnotation.name(), ((HTMLContainer) field.get(page)).getWrappedElement());
                } else return;
            } catch (IllegalAccessException e) {
                System.err.println("Caught IllegalAccessException related to 'HTMLContainer' annotation");
            }
        }
    }

    /**
     * Use when page initializes from WebElement type container (WebElement container object as parameter)
     *
     * @param page Current page object
     */
    public static void containerInitFromWebElement(Object page) {
        collectContainers(page, true);
        containerInitElementsDecorator(page);
    }

    /**
     * Use when page initializes with @FindBy or @FindIn initialized container
     *
     * @param page Current page object
     */
    public static void containerInitWithAnnotation(Object page) {
        initElements(getDriver(), page);
        collectContainers(page, false);
        containerInitElementsDecorator(page);
    }

    /**
     * Use when page initializes for using HTMLElement objects
     * {@link #'com.thomsonreuters.cucumberfactory.typesfactory.types.HTMLElement'}
     * Elements must be marked by @FindBy or @FindIn annotation.
     *
     * @param page Current page object
     */
    public static void containerInitHTMLElements(Object page) {
        initHTMLContainers(getDriver(), page);
        collectHTMLContainers(page);
        containerInitHTMLElementsDecorator(page);
    }

    /**
     * This method is used to decorate custom fields on the page.
     * @param page
     */
    private static void containerInitElementsDecorator(Object page) {
        Map<String, WebElement> containerInternal = containerMap;
        initElements(new CustomFieldDecorator(containerInternal), page);
        containerMap.clear();
    }

    /**
     * This method is used to decorate custom fields on the page, that has @FindIn annotation,
     * to create locator for custom element based on HTMLContainer object.
     * @param page
     */
    private static void containerInitHTMLElementsDecorator(Object page) {
        Map<String, WebElement> containerInternal = containerMap;
        initElements(new CustomHTMLFieldDecorator(containerInternal), page);
//        containerMap.clear();
    }
}
