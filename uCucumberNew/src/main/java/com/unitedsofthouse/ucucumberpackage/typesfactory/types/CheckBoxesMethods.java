package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class CheckBoxesMethods extends Element {

    /**
     * Constructor
     * @param element
     */
    public CheckBoxesMethods(WebElement element) {
        super(element);
    }

    /**
     * Constructor
     * @param element
     * @param name
     */
    public CheckBoxesMethods(WebElement element, String name) {
        super(element, name);
    }

    /**
     * Constructor
     * @param by
     */
    public CheckBoxesMethods(By by) {
        super(by);
    }

    /**
     * This method is used to select checkbox
     * @throws Exception
     */
    public void check() throws Exception {
        check(true);
    }

    /**
     * This method is used to select checkbox
     * @param expected
     * @throws Exception
     */
    public void check(boolean expected) throws Exception {
        if (!isSelected()) {
            TypeFactory.getArpReportClient().ReportAction(String.format("Check %s.", getName()), true);
            if (invokers != null) {
                WebElement element = getWrappedElement();
                //BEFORE CLICK
                invokers.beforeClick(element);
                //ACTION
                element.click();
                //AFTER CLICK
                invokers.afterClick(element);
            } else getWrappedElement().click();
        }
        validate(isSelected() == expected, "checked.");
    }

    /**
     * This method is used to select checkbox and validate
     * @param expected
     * @param validate
     * @throws Exception
     */
    public void check(boolean expected, boolean validate) throws Exception {
        if (!isSelected()) {
            TypeFactory.getArpReportClient().ReportAction(String.format("Check %s.", getName()), true);
            if (invokers != null) {
                WebElement element = getWrappedElement();
                //BEFORE CLICK
                invokers.beforeClick(element);
                //ACTION
                element.click();
                //AFTER CLICK
                invokers.afterClick(element);
            } else getWrappedElement().click();
        }
        if (validate) {
            validate(isSelected() == expected, "checked.");
        }
    }

    /**
     * This method is used to uncheck checkbox
     * @throws Exception
     */
    public void uncheck() throws Exception {
        uncheck(true);
    }

    /**
     * This method is used to uncheck checkbox
     * @param expected
     * @throws Exception
     */
    public void uncheck(boolean expected) throws Exception {
        if (isSelected()) {
            TypeFactory.getArpReportClient().ReportAction(String.format("Uncheck %s.", getName()), true);
            if (invokers != null) {
                WebElement element = getWrappedElement();
                //BEFORE CLICK
                invokers.beforeClick(element);
                //ACTION
                element.click();
                //AFTER CLICK
                invokers.afterClick(element);
            } else getWrappedElement().click();
        }
        validate(!isSelected() == expected, "unchecked.");
    }

    /**
     * This method is used to uncheck checkbox and validate
     * @param expected
     * @param validate
     * @throws Exception
     */
    public void uncheck(boolean expected, boolean validate) throws Exception {
        if (isSelected()) {
            TypeFactory.getArpReportClient().ReportAction(String.format("Uncheck %s.", getName()), true);
            if (invokers != null) {
                WebElement element = getWrappedElement();
                //BEFORE CLICK
                invokers.beforeClick(element);
                //ACTION
                element.click();
                //AFTER CLICK
                invokers.afterClick(element);
            } else getWrappedElement().click();
        }
        if (validate) {
            validate(!isSelected() == expected, "unchecked.");
        }
    }

    /**
     * This method is used to get label of checkbox
     * @return String Label of checkbox
     * @throws Exception
     */
    public String labelText() throws Exception {
        String labelText = "";
        TypeFactory.getArpReportClient().ReportAction(String.format("Get label text for %s.", getName()), true);
        WebElement element = getWrappedElement();
        if (invokers != null) {
            labelText = invokers.getLabelText(element);
        }
        labelText = element.getText();
        TypeFactory.getArpReportClient().ReportAction(String.format("text of label for %s : %s.", getName(), labelText), true);
        return labelText;
    }
}
