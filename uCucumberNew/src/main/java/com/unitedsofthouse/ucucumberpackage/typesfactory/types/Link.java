package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class Link extends Element {
    /**
     * Constructor
     * @param element web element
     */
    public Link(WebElement element) {
        super(element);
    }
    /**
     * Constructor
     * @param element web element
     * @param name name of element
     */
    public Link(WebElement element, String name) {
        super(element, name);
    }
    /**
     * Constructor
     * @param by class object
     */
    public Link(By by) {
        super(by);
    }
    /**
     * Method is used to get value of href attribute
     * @return String with link href
     */
    public String getHref() {
        return getWrappedElement().getAttribute("href");
    }
}
