package com.unitedsofthouse.ucucumberpackage.typesfactory.factory.fielddecorator;

import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.Container;
import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.FindIn;
import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.ElementName;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.elementlocatorpackage.CustomElementLocatorFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.FactoryUtils;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.Proxies;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static com.unitedsofthouse.ucucumberpackage.tools.WebCucDriver.getDriver;
import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.FactoryUtils.getErasureClass;
/**
 * Created by pchelintsev on 12/7/2015.
 * Used this class to initialize only pages elements (need to test)
 */
public class CustomHTMLFieldDecorator extends DefaultFieldDecorator {
    private Map<String, WebElement> containers = new HashMap<>();

    /**
     * Constructor
     * @param container_Map
     */
    public CustomHTMLFieldDecorator(Map<String, WebElement> container_Map) {
        super(new CustomElementLocatorFactory(container_Map));
        containers = container_Map;
    }

    /**
     * fabric invoke this method for each field in class
     *
     * @param loader
     * @param field
     * @return Object
     */
    public Object decorate(ClassLoader loader, Field field) {

        String name;
        boolean isDecoratableList = false;
        Class<?> elementType = field.getType();
        Annotation annotations = field.getAnnotation(Container.class);
        if (annotations != null)
            return null;

        boolean isAssignableFromElementType = Element.class.isAssignableFrom(elementType);
        isDecoratableList = FactoryUtils.isDecoratableList(Element.class, field);

        if (isAssignableFromElementType || isDecoratableList) {

            for (Annotation annotation : field.getDeclaredAnnotations()) {
                if (annotation.annotationType() == FindIn.class) {
                    factory = new CustomElementLocatorFactory(containers);
                    break;
                } else if (annotation.annotationType() == FindBy.class) {
                    factory = new DefaultElementLocatorFactory(getDriver());
                    break;
                }
            }

            ElementLocator locator = factory.createLocator(field);
            if (locator == null) {
                return null;
            }

            annotations = field.getAnnotation(ElementName.class);
            if (annotations != null) {
                name = ((ElementName) annotations).value();
            } else {
                name = field.getName().replace('_', ' ');
            }

            if (isAssignableFromElementType) {
                @SuppressWarnings(value = "unchecked")
                Class<Element> element = (Class<Element>) field.getType();
                return Proxies.HTMLProxyForLocator(loader, locator, element, name);
            } else if (isDecoratableList) {
                @SuppressWarnings(value = "unchecked")
                Class<Element> erasureClass = (Class<Element>) getErasureClass(field);
                return Proxies.HTMLProxyForListLocator(loader, locator, erasureClass, name);
            }
        }
        return super.decorate(loader, field);
    }
}
