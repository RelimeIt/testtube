package com.unitedsofthouse.ucucumberpackage.typesfactory.annotations;

/**
 * enum to find web elements by: XPath, CSSSelector, ID or NAME
 */
public enum  FindWith
{
    XPath,
    CSSSelector,
    ID,
    NAME
}