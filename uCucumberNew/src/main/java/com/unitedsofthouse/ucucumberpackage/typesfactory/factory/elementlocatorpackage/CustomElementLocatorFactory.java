package com.unitedsofthouse.ucucumberpackage.typesfactory.factory.elementlocatorpackage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class CustomElementLocatorFactory implements ElementLocatorFactory {

    private Map<String, WebElement> containers = new HashMap<>();

    /**
     * Constructor
     * @param containers
     */
    public CustomElementLocatorFactory(Map<String, WebElement> containers) {
        this.containers = containers;
    }

    @Override
    public ElementLocator createLocator(Field field) {
        return new CustomElementLocator(field, containers);
    }
}
