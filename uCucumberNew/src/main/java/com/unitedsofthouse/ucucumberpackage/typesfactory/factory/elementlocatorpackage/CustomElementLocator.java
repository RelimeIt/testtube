package com.unitedsofthouse.ucucumberpackage.typesfactory.factory.elementlocatorpackage;

import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.FindIn;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.DefaultElementLocator;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class CustomElementLocator implements ElementLocator{
    private Field _field;
    private By locator;
    private Map<String, WebElement> containers = new HashMap<>();

    /**
     * Constructor
     * @param field
     * @param _containers
     */
    public CustomElementLocator(Field field, Map<String, WebElement> _containers) {
        this.containers = _containers;
        this._field = field;
    }

    /**
     * This method is used to set HTML containers
     * @param _containers
     * @return
     */
    public CustomElementLocator setContainers(Map<String, WebElement> _containers)
    {
        this.containers = _containers;
        return this;
    }

    /**
     * This method is used to define what type of locator will be used to find custom web element
     * @param fieldAnnotation
     */
    private void fillLocator(FindIn fieldAnnotation)
    {
        String value = fieldAnnotation.value();

        switch (fieldAnnotation.findWith()){
            case XPath: locator = By.xpath(value);
                break;
            case CSSSelector: locator = By.cssSelector(value);
                break;
            case ID: locator = By.id(value);
                break;
            case NAME: locator = By.name(value);
            default:
                locator = null;
                break;
        }
    }

    /**
     * This method is used to find custom element that has @FindIn annotation and based on custom HTML container
     * @return WebElement
     */
    public WebElement findElement() {
        if(_field.isAnnotationPresent(FindIn.class))
        {
            final FindIn fieldAnnotation = _field.getAnnotation(FindIn.class);
            fillLocator(fieldAnnotation);
            try {
                return containers.get(fieldAnnotation.containerName()).findElement(locator);
            } catch (NoSuchElementException e){
                return null;
            }
        }
        return null;
    }

    /**
     * This method is used to find list of custom elements that has @FindIn annotation and based on custom HTML container
     * @return List<WebElement>
     */
    public List<WebElement> findElements() {
        if(_field.isAnnotationPresent(FindIn.class))
        {
            final FindIn fieldAnnotation = _field.getAnnotation(FindIn.class);
            fillLocator(fieldAnnotation);
            return containers.get(fieldAnnotation.containerName()).findElements(locator);
        }
        return null;
    }
}
