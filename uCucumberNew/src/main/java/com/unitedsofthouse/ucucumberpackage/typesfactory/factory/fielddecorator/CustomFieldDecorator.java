package com.unitedsofthouse.ucucumberpackage.typesfactory.factory.fielddecorator;

import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.Container;
import com.unitedsofthouse.ucucumberpackage.typesfactory.annotations.FindIn;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.elementlocatorpackage.CustomElementLocatorFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.FactoryUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.DefaultFieldDecorator;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pchelintsev on 12/7/2015.
 */

/**
 * Initialize fields in Page Object classes, annotated by @FindIn
 * Used this class to initialize only popup elements (need to test)
 */

public class CustomFieldDecorator extends DefaultFieldDecorator {
    private Map<String, WebElement> containers = new HashMap<>();

    /**
     * Constructor
     * @param container_Map
     */
    public CustomFieldDecorator(Map<String, WebElement> container_Map) {
        super(new CustomElementLocatorFactory(container_Map));
        containers = container_Map;
    }

    /**
     * Method invokes by fabric for each field in class
     *
     * @param loader
     * @param field
     * @return Object
     */
    public Object decorate(ClassLoader loader, Field field) {

        boolean isDecoratableList = false;
        Class<?> elementType = field.getType();
        Annotation annotations = field.getAnnotation(Container.class);
        if (annotations != null)
            return null;

        boolean isAssignableFromElementType = WebElement.class.isAssignableFrom(elementType);
        isDecoratableList = FactoryUtils.isDecoratableList(WebElement.class, field);

        if (isAssignableFromElementType || isDecoratableList) {

            for (Annotation annotation : field.getDeclaredAnnotations()) {
                if (annotation.annotationType() == FindIn.class) {
                    ElementLocator locator = factory.createLocator(field);
                    if (locator == null) return null;

                    if (isAssignableFromElementType)
                        return super.proxyForLocator(loader, locator);
                    else if (isDecoratableList)
                        return super.proxyForListLocator(loader, locator);
                }
            }
        }
        return null;
    }
}
