package com.unitedsofthouse.ucucumberpackage.typesfactory.factory.invocationhandlers;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import com.unitedsofthouse.ucucumberpackage.typesfactory.types.Element;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.NoSuchElementException;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class HTMLInvocationHandler<T extends Element> implements InvocationHandler {
    private final ElementLocator locator;
    private final String name;

    public HTMLInvocationHandler(ElementLocator elementLocator, String name) {
        locator = elementLocator;
        this.name = name;
    }

    /**
     * Looking list of web Elements, process it and returns
     * new list with Custom type elements
     * @param object
     * @param method
     * @param objects
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object object, Method method, Object[] objects) throws Throwable {
        WebElement rootElement = locator.findElement();

        if (rootElement == null && ("isEnabled".equals(method.getName()) || "isDisplayed".equals(method.getName()))) {
            return false;
        } else if ("getWrappedElement".equals(method.getName())) {
            return rootElement;
        } else if (rootElement == null) {
            throw new NoSuchElementException("Cannot locate an element using " + toString());
        }
        try {
            return method.invoke(rootElement, objects);
        } catch (InvocationTargetException e) {
            TypeFactory.getArpReportClient().ReportAction(name + " can't be created.", false);
            throw e.getCause();
        }
    }
}