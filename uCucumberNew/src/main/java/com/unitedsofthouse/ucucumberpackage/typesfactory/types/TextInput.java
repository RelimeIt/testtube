package com.unitedsofthouse.ucucumberpackage.typesfactory.types;

import com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.stringtemplate.v4.StringRenderer.escapeHTML;

/**
 * Created by pchelintsev on 12/7/2015.
 */
public class TextInput extends Element {
    /**
     * Constructor
     * @param element
     */
    public TextInput(WebElement element) {
        super(element);
    }

    /**
     * Constructor
     * @param element
     * @param name
     */
    public TextInput(WebElement element, String name) {
        super(element, name);
    }

    /**
     * Constructor
     * @param by
     */
    public TextInput(By by) {
        super(by);
    }

    /**
     * Method is used to set text to field
     * @param text Value that we want to set to field
     * @throws Exception
     */
    public void enterText(String text) throws Exception {
        enterText(text, true, true);
    }

    /**
     * Method is used to set text to field
     * @param text Value that we want to set to field
     * @param clearBefore
     * @param expected
     * @throws Exception
     */
    public void enterText(String text, boolean clearBefore, boolean expected) throws Exception {
        TypeFactory.getArpReportClient().ReportAction("Enter text '"+ text +"' to "+ getName(), true);
        WebElement element = getWrappedElement();
        if (clearBefore) {
            clear(true);
        }
        int length = this.getText().length();
        if (invokers != null) {
            invokers.enterText(element, text);
        } else {
            element.sendKeys(text);
        }
        validate((getText().length() == length + text.length()) == expected, String.format("'%s' text was entered.", text));
    }

    /**
     * Method is used to get text from field
     * If readText invoker is set to the element, then invoker will be applied.
     * @return String Value from input field
     * @throws Exception
     */
    @Override
    public String getText() throws Exception {
        String text = "";
        TypeFactory.getArpReportClient().ReportAction(String.format("Get text for %s.", getName()), true);
        WebElement element = getWrappedElement();
        if (invokers != null) {
            text = invokers.readText(escapeHTML(element.getText()));
        } /*else if (StringUtils.isNoneBlank(element.getText())){
                return escapeHTML(getWrappedElement().getText());
        }*/ else
            text = getWrappedElement().getAttribute("value");
        TypeFactory.getArpReportClient().ReportAction(String.format("text of %s is %s.", getName(), text), true);
        return text;
    }

    /**
     * @throws Exception
     */
    public void clear() throws Exception {
        clear(true);
    }

    /**
     * Method is used to clear field
     * @param expected
     * @throws Exception
     */
    public void clear(boolean expected) throws Exception {
        TypeFactory.getArpReportClient().ReportAction("Clear " + getName(), true);
        getWrappedElement().clear();
        validate(getWrappedElement().getAttribute("value").isEmpty() == expected, "cleared.");
    }
}
