package com.unitedsofthouse.ucucumberpackage.typesfactory.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * interface used to set for web element name
 * maybe, once, we want to call web element by name
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ElementName {
    //value of name web element
    String value() default "";
}
