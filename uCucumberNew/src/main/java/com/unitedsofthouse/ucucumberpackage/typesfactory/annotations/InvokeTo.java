package com.unitedsofthouse.ucucumberpackage.typesfactory.annotations;

        import java.lang.annotation.ElementType;
        import java.lang.annotation.Retention;
        import java.lang.annotation.RetentionPolicy;
        import java.lang.annotation.Target;

/**
 * interface InvokeTo - to get field name which contains invokers
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface InvokeTo {
    //return field name
    String elemet();
}