package com.unitedsofthouse.ucucumberpackage.typesfactory.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * interface FindIn - find web elements in the container
 * used to find web elements in difficult of approach places
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface FindIn
{
    //find element by XPath
    FindWith findWith() default FindWith.XPath;
    //element value
    String value() default "";
    //find element in the container
    String containerName() default "";
}