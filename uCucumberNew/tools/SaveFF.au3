Local $count = 0
Local $stext = ""
While $count < 120 AND WinExists("Opening") == 0
	Sleep(1000)
	$count = $count + 1
WEnd
WinActivate("Opening")
$count = 0
While $count < 5 AND WinExists("Opening")
	If WinExists("[CLASS:MozillaDialogClass;]", "") Then
		$wintitle = WinGetTitle("[CLASS:MozillaDialogClass;]", "")
		If StringMid($wintitle, 1, 7) = "Opening" Then
			$winpos = WinGetCaretPos()
			If NOT @error Then
				Send("{LEFT}")
				Send("!s")
				Sleep(100)
				Send("{ENTER}")
			EndIf
		EndIf
	EndIf
	$count = $count + 1
	Sleep(3000)
WEnd
$stext = entertexttosaveas($cmdline[2])
Sleep(1000)
$mainhandle = WinGetHandle("Enter name of file to save to&")
While $count < 10 AND WinExists("Enter name of file to save to&")
	WinActivate("Enter name of file to save to&")
	Send("{ENTER}")
	Sleep(2000)
	If WinExists("Enter name of file to save to&") Then
		$handle = WinGetHandle("Enter name of file to save to&")
		If $mainhandle <> $handle Then
			Send("{ENTER}")
			If ($stext == "") Then
				$stext = entertexttosaveas($cmdline[2])
			Else
				entertexttosaveas($cmdline[2])
			EndIf
		EndIf
	EndIf
	$count = $count + 1
	Sleep(1000)
WEnd
If FileExists(@ScriptDir & "\Export_FileName.txt") Then
	Local $file = FileOpen(@ScriptDir & "\Export_FileName.txt", 1)
	If $file = -1 Then
		Exit 
	EndIf
	FileWriteLine($file, $stext)
	FileClose($file)
EndIf
Exit 

Func entertexttosaveas($text)
	WinWait("Enter name of file to save to&", "", 100)
	WinActivate("Enter name of file to save to&")
	Sleep(2000)
	Local $stexttoreturn = ControlGetText("Save As", "", "[CLASS:Edit]")
	ControlSetText("Enter name of file to save to&", "", "[CLASS:Edit]", "")
	Sleep(2000)
	Send($text)
	Return $stexttoreturn
EndFunc
