Local $count = 0
Local $stext = ""
While $count < 120 AND WinExists("Save As") == 0
	Sleep(1000)
	$count = $count + 1
WEnd
WinActivate("Save As")
$stext = entertexttosaveas($cmdline[2])
$count = 0
Sleep(1000)
$mainhandle = WinGetHandle("Save As")
While $count < 10 AND WinExists("Save As")
	WinActivate("Save As")
	Send("{ENTER}")
	Sleep(2000)
	$count = $count + 1
	Sleep(1000)
WEnd
If FileExists(@ScriptDir & "\Export_FileName.txt") Then
	Local $file = FileOpen(@ScriptDir & "\Export_FileName.txt", 1)
	If $file = -1 Then
		Exit
	EndIf
	FileWriteLine($file, $stext)
	FileClose($file)
EndIf
Exit

Func entertexttosaveas($text)
	WinWait("Save As", "", 100)
	WinActivate("Save As")
	Sleep(2000)
	Local $stexttoreturn = ControlGetText("Save As", "", "[CLASS:Edit]")
	ControlSetText("Save As", "", "[CLASS:Edit]", "")
	Sleep(2000)
	Send($text)
	Return $stexttoreturn
EndFunc